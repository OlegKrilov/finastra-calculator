const
  express = require('express'),
  bodyParser = require('body-parser'),
  {parsed: config} = require('dotenv').config(),
  dataCtrl = require('./private/data.ctrl');

const
  app = express();

app.use(bodyParser.json({
  parameterLimit: 500000,
  limit: '50mb',
  extended: true
}));

app.use(bodyParser.urlencoded({
  parameterLimit: 1000000,
  limit: '100mb',
  extended: false
}));

/** Controllers */
dataCtrl(app);

/** Run server */
app.listen(config.API_PORT, () => console.log(`Application is running on port: ${config.API_PORT}`));

