import {FC_ButtonModel} from "../Components/FC_Button/FC_Button.model";
import {FC_DropdownModel, I_FC_DropdownItem} from "../Components/FC_Dropdown/FC_Dropdown.model";
import {COMPONENT, ICON, ID, LABEL, TYPE} from "../Core/Constants/PropertiesAndAttributes.cnst";
import {INPUT_TYPES, LOAN_TYPES, MAIN_FORM_FIELDS} from "../Constatns/FC_Constants";
import {CHECK_AND_CALL, GET_DEEP_PROPERTY, GET_TRUE, PARSE_CAMEL_CASE} from "../Core/Helpers/Helpers.misc";
import {AbstractCollection} from "../Core/Abstract/AbstractCollection";
import {SwitchObservable} from "../Core/Observables/SwitchObservable";
import {FC_NumberInputModel} from "../Components/FC_NumberInput/FC_NumberInput.model";
import {FC_DateSelectorModel} from "../Components/FC_DateSelector/FC_DateSelector.model";
import React from "react";
import {IS_REQUIRED} from "../Core/Constants/ViewClasses.cnst";
import {computed} from "mobx";
import {BIG_NUMBER} from "../Constatns/FC_Formatters";
import {INT_NUMBER, NUMBER_IS_IN_RANGE} from "../Constatns/FC_Validators";

export interface I_FC_MainFormField {
  [ID]: string,
  [TYPE]: string,
  [COMPONENT]: any,
  [IS_REQUIRED]: SwitchObservable
}

export class SharedService {

  /** Header */
  public readonly loanTypeSelector: FC_DropdownModel = new FC_DropdownModel().addItems(
    Object.keys(LOAN_TYPES).map(key => ({
      [ID]: LOAN_TYPES[key],
      [LABEL]: PARSE_CAMEL_CASE(LOAN_TYPES[key])
    } as I_FC_DropdownItem))
  ).select(LOAN_TYPES.PRIVATE_LOAN);

  public readonly downloadExcelButton: FC_ButtonModel = new FC_ButtonModel();

  public readonly shareButton: FC_ButtonModel = new FC_ButtonModel();

  /** Main */
  public readonly mainForm: AbstractCollection = new AbstractCollection();

  public readonly formWasSubmitted: SwitchObservable = new SwitchObservable();

  /** Globals */
  public readonly isLoading: SwitchObservable = new SwitchObservable(true);

  /** Public methods */
  public clearForm = (filter: any = GET_TRUE): SharedService => {
    this.mainForm.items
      .filter((d: I_FC_MainFormField) => CHECK_AND_CALL(filter, d))
      .forEach((d: I_FC_MainFormField) => {
        d[COMPONENT].clearValue();
        d[IS_REQUIRED].resetValue();
      });

    return this;
  };

  public checkValidity = () => this.mainForm.items
    .filter((d: I_FC_MainFormField) => d[IS_REQUIRED].value)
    .every((d: I_FC_MainFormField) => d[COMPONENT].currentValue);

  @computed
  public get isValid () {
    return this.checkValidity();
  }

  @computed
  public get formValue () {
    return this.mainForm.items.reduce((agg: any, d: I_FC_MainFormField) => {
      const {currentValue} = d[COMPONENT];

      switch (d[TYPE]) {
        case INPUT_TYPES.DROPDOWN: agg[d[ID]] = GET_DEEP_PROPERTY(currentValue, ID); break;
        case INPUT_TYPES.DATE_SELECTOR: agg[d[ID]] = new Date(currentValue); break;
        default: agg[d[ID]] = currentValue; break;
      }

      return agg;
    }, {});
  }

  /** Private Methods */
  private _refreshForm = () => {
    const
      bigNumberFilter = BIG_NUMBER();

    let
      amountBorrowedValue: FC_NumberInputModel,
      amountBorrowedCurrency: FC_DropdownModel,
      startDate: FC_DateSelectorModel,
      interestRateValue: FC_NumberInputModel,
      additionalInterestRateDate: FC_DateSelectorModel,
      additionalInterestRateValue: FC_NumberInputModel,
      durationYears: FC_NumberInputModel,
      durationMonths: FC_NumberInputModel,
      rateBasis: FC_DropdownModel,
      repaymentPlan: FC_DropdownModel;

    amountBorrowedValue =  new FC_NumberInputModel({
      getIcon: () => GET_DEEP_PROPERTY(amountBorrowedCurrency.items.selection, ICON),
      getFormattedLabel: bigNumberFilter
    });

    amountBorrowedCurrency = new FC_DropdownModel({}, 'USD').addItems([
      {
        [ID]: 'USD',
        [LABEL]: 'Dollars (USA)',
        [ICON]: '$'
      },
      {
        [ID]: 'UKP',
        [LABEL]: 'Pound (UK)',
        [ICON]: '£'
      },
      {
        [ID]: 'EURO',
        [LABEL]: 'Euro',
        [ICON]: '€'
      }
    ]);

    startDate = new FC_DateSelectorModel({}, new Date().getTime());

    interestRateValue = new FC_NumberInputModel({
      getIcon: '%',
      getFormattedLabel: bigNumberFilter,
      validators: NUMBER_IS_IN_RANGE(0, 10,
      val => {
        let
          _text = '',
          _val = val;

        if (val < 0) {
          _text += 'Input Interest Rate';
          _val = 0;
        }
        if (val > 10) {
          _text += 'Interest Rate should be equal or less then 10';
          _val = 10;
        }
        interestRateValue
          .setValue(_val)
          .setError(<small>{_text}</small>);
      })
    }, 1.2);

    additionalInterestRateDate = new FC_DateSelectorModel({
      placeholder: 'Date start of interest rate'
    });

    additionalInterestRateValue = new FC_NumberInputModel({
      placeholder: 'Interest rate',
      getIcon: '%'
    });

    durationYears = new FC_NumberInputModel({
      getIcon: <small>year(s)</small>,
      validators: [
        INT_NUMBER(
          () => durationYears.setError(<small>Number of years should be an Integer</small>)
        ),
        NUMBER_IS_IN_RANGE(0, 10,
          (val) => {
            let
              _text = '',
              _val = val;

            if (val < 0) {
              _text += 'Input number of years';
              _val = 0;
            }
            if (val > 10) {
              _text += 'Number of years should be equal or less then 10';
              _val = 10;
            }
            durationYears
              .setValue(_val)
              .setError(<small>{_text}</small>)
          }
        )
      ]
    });

    durationMonths = new FC_NumberInputModel({
      getIcon: <small>month(s)</small>,
      validators: [
        INT_NUMBER(
          () => durationMonths.setError(<small>Number of months should be an Integer</small>)
        ),
        NUMBER_IS_IN_RANGE(0, 11,
          val => {
            let
              _text = '',
              _val = val;

            if (val < 0) {
              _text += 'Input number of months';
              _val = 0;
            }
            if (val > 10) {
              _text += 'Number of months should be equal or less then 11';
              _val = 10;
            }
            durationMonths
              .setValue(_val)
              .setError(<small>{_text}</small>);
        })
      ]
    });

    rateBasis = new FC_DropdownModel().addItems([
      'Actual/252',
      'Actual/365',
      'Actual/365/366',
      'Bank Actual/360',
      'Bond 30/360',
      'BondE 30/360',
      'Bond Actual/(12 30-Day Months)',
      'Modified 30/360',
      'Modified 30E/360'
    ]);

    repaymentPlan = new FC_DropdownModel().addItems([
      'Fixed',
      'Variable',
      'Fixed-indexed',
      'Immediate',
      'Deferred'
    ]);

    this.mainForm.addItems([
      {
        [ID]: MAIN_FORM_FIELDS.AMOUNT_BORROWED_VALUE,
        [TYPE]: INPUT_TYPES.NUMBER_INPUT,
        [COMPONENT]: amountBorrowedValue,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.AMOUNT_BORROWED_CURRENCY,
        [TYPE]: INPUT_TYPES.DROPDOWN,
        [COMPONENT]: amountBorrowedCurrency,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.START_DATE,
        [TYPE]: INPUT_TYPES.DATE_SELECTOR,
        [COMPONENT]: startDate,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.INTEREST_RATE,
        [TYPE]: INPUT_TYPES.NUMBER_INPUT,
        [COMPONENT]: interestRateValue,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.ADDITIONAL_INTEREST_RATE_DATE,
        [TYPE]: INPUT_TYPES.DATE_SELECTOR,
        [COMPONENT]: additionalInterestRateDate,
        [IS_REQUIRED]: new SwitchObservable(false)
      },
      {
        [ID]: MAIN_FORM_FIELDS.ADDITIONAL_INTEREST_RATE_VALUE,
        [TYPE]: INPUT_TYPES.NUMBER_INPUT,
        [COMPONENT]: additionalInterestRateValue,
        [IS_REQUIRED]: new SwitchObservable(false)
      },
      {
        [ID]: MAIN_FORM_FIELDS.DURATION_MONTHS,
        [TYPE]: INPUT_TYPES.NUMBER_INPUT,
        [COMPONENT]: durationMonths,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.DURATION_YEARS,
        [TYPE]: INPUT_TYPES.NUMBER_INPUT,
        [COMPONENT]: durationYears,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.RATE_BASIS,
        [TYPE]: INPUT_TYPES.DROPDOWN,
        [COMPONENT]: rateBasis,
        [IS_REQUIRED]: new SwitchObservable(true)
      },
      {
        [ID]: MAIN_FORM_FIELDS.REPAYMENT_PLAN,
        [TYPE]: INPUT_TYPES.DROPDOWN,
        [COMPONENT]: repaymentPlan,
        [IS_REQUIRED]: new SwitchObservable(true)
      }
    ] as I_FC_MainFormField[]);
  };

  constructor () {
    this._refreshForm();
  }

}

export default new SharedService();