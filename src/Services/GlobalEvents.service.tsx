import * as d3 from 'd3';
import {AbstractEventObserver} from "../Core/Observers/AbstractEventObserver";

export class GlobalEventsService {

  public onMouseDown: AbstractEventObserver = new AbstractEventObserver('mouseDown');

  public onScroll: AbstractEventObserver = new AbstractEventObserver('scroll');

  constructor () {
    d3.select(document).on('mousedown', () => this.onMouseDown.next(d3.event));
  }
}

export default new GlobalEventsService();