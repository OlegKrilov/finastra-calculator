import axios from 'axios';
import {API_ROUTES} from "../Constatns/FC_Constants";
import {StructureObservable} from "../Core/Observables/StructureObservable";
import {computed} from "mobx";

export class DataService {

  public data: StructureObservable = new StructureObservable();

  public sendGet = (url: string, params: any = {}, headers: any = {}) => new Promise(
    (resolve, reject) => axios.get(url, {params, headers})
      .then(res => resolve(res.data))
      .catch(err => reject(err))
  );

  public sendPost = (url: string, data: any = {}, headers: any = {}) => new Promise(
    (resolve, reject) => axios.post(url, data, {headers})
      .then(res => resolve(res.data))
      .catch(err => reject(err))
  );

  public calculate = (data) => {
    this.data.resetValue();
    return new Promise(
      (resolve, reject) => this.sendPost(API_ROUTES.CALCULATE, data)
        .then(data => {
          this.data.setValue(data);


          resolve(data);
        })
        .catch(err => reject(err))
    );
  };

  @computed
  public get hasLoadedData (): boolean {
    return !this.data.isEmpty;
  }
}

export default new DataService();