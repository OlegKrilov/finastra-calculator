import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {FC_GridModel, I_FC_GridColumn} from "./FC_Grid.model";
import {CONTENT, LABEL, NAME, WIDTH} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {CHECK_AND_CALL, GET_DEEP_PROPERTY, IS_FUNCTION} from "../../Core/Helpers/Helpers.misc";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";

const
  ROOT = 'fc-grid',
  COLS = `${ROOT}-cols`,
  COL = `${ROOT}-col`,
  ROWS = `${ROOT}-rows`,
  ROW = `${ROOT}-row`,
  CELL = `${ROOT}-cell`;

export interface I_FC_GridView {
  cols: any[],
  rows: any[]
}

@observer
export class FC_Grid extends AbstractComponent {

  private readonly model: FC_GridModel;

  private readonly _sizes: StructureObservable = new StructureObservable();

  private readonly _view: StructureObservable = new StructureObservable({
    cols: [],
    rows: []
  } as I_FC_GridView);

  // private readonly _onChange: AbstractEventObserver = new AbstractEventObserver('change');

  private _getCols = () => {
    let
      {model, _sizes} = this,
      {cols} = model,
      sizes = {},
      total = cols.items.reduce((val: number, d: I_FC_GridColumn) => {
        sizes[d[NAME]] = d[WIDTH] || 1;
        return val + sizes[d[NAME]];
      }, 0);

    Object.keys(sizes).forEach(key => sizes[key] = (100 / total) * sizes[key]);
    _sizes.setValue(sizes);

    return cols.items.map((d: I_FC_GridColumn, i: number) =>
      <div className={`${COL} display-inline-flex`} key={i} style={{'width': `${GET_DEEP_PROPERTY(_sizes.value, d[NAME])}%`}}>{
        IS_FUNCTION(d[LABEL]) ? CHECK_AND_CALL(d[LABEL], d) : d[LABEL]
      }</div>
    );
  };

  private _getRows = () => {
    const
      {model, _sizes} = this,
      {rows, cols} = model;

    return rows.items.map((d: any, i: number) =>
      <div className={`${ROW}`} key={i}>{
        cols.items.map((dd: I_FC_GridColumn, ii: number) =>
          <div className={`${CELL} display-inline-flex`} key={ii} style={{'width': `${GET_DEEP_PROPERTY(_sizes.value, dd[NAME])}%`}}>{
            IS_FUNCTION(dd[CONTENT]) ? CHECK_AND_CALL(dd[CONTENT], d) : GET_DEEP_PROPERTY(d, dd[NAME])
          }</div>
        )
      }</div>
    );
  };

  componentDidMount(): void {
    const
      {model, _view} = this,
      {cols, rows} = model;

    const
      _refreshView = () =>  this._view.setValue({
        cols: this._getCols(),
        rows: this._getRows()
      });

    this.registerSubscriptions(
      cols.subscribeOnCollectionChange(() => _refreshView()),
      rows.subscribeOnCollectionChange(() => _refreshView())
    );
  }

  render () {
    const view: I_FC_GridView = this._view.value;

    console.log(view);

    return <div className={`${ROOT}`}>
      <div className={`${COLS}`}>{view.cols}</div>
      <div className={`${ROWS}`}>{view.rows}</div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}

