import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {CONTENT, ID, LABEL, NAME, WIDTH} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import React from "react";

export interface I_FC_GridColumn {
  [NAME]?: string | number | any,
  [LABEL]?: string | any,
  [CONTENT]?: any,
  [WIDTH]?: number
}

export class FC_GridModel {

  public cols: AbstractCollection = new AbstractCollection(NAME);

  public rows: AbstractCollection = new AbstractCollection(ID);

  public refreshCols (cols): FC_GridModel {
    this.cols.replaceItems(cols);
    return this;
  }

  public refreshRows (rows): FC_GridModel {
    this.rows.replaceItems(rows);
    return this;
  }

  public clearRows (): FC_GridModel {
    this.rows.clearItems();
    return this;
  }

}