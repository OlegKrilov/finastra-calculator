import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {FC_DateSelectorModel} from "./FC_DateSelector.model";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {TIME_PERIODS} from "../../Constatns/FC_Constants";
import {ACTION, CHILDREN, ID, LABEL, STEP} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EMPTY_STRING, IS_DISABLED, IS_OPENED} from "../../Core/Constants/ViewClasses.cnst";
import {FC_DropdownBody} from "../FC_DropdownBody/FC_DropdownBody";
import {FC_DateSelectorBody} from "./FC_DateSelectorBody";
import {GET_DEEP_PROPERTY, IS_NULL} from "../../Core/Helpers/Helpers.misc";
import moment from "moment";
import {IoMdCalendar} from "react-icons/all";
import {NumberObservable} from "../../Core/Observables/NumberObservable";
import {AS_TIMESTAMP, START_OF} from "../../Constatns/FC_Helpers";

const
  ROOT = 'fc-date-selector',
  HEADER = `${ROOT}-header`,
  BODY_BASE = `${ROOT}-body-base`,
  INPUT_LABEL = `${HEADER}-label`,
  INPUT_ICON = `${HEADER}-icon`;

@observer
export class FC_DateSelector extends AbstractComponent {

  private readonly model: FC_DateSelectorModel;

  private readonly baseDate: NumberObservable = new NumberObservable();

  private readonly views: AbstractCollection = new AbstractCollection()
    .addItems([
      {
        [ID]:TIME_PERIODS.DAY,
        [ACTION]: () => this.views.select(TIME_PERIODS.MONTH),
        [LABEL]: () => moment(this.baseDate.value).format('MMM YYYY'),
        [STEP]: [1, TIME_PERIODS.MONTH]
      },
      {
        [ID]:TIME_PERIODS.MONTH,
        [ACTION]: () => this.views.select(TIME_PERIODS.YEAR),
        [LABEL]: () => moment(this.baseDate.value).format('YYYY'),
        [STEP]: [1, TIME_PERIODS.YEAR]
      },
      {
        [ID]:TIME_PERIODS.YEAR,
        [ACTION]: () => this.views.select(TIME_PERIODS.DAY),
        [LABEL]: () => {
          const
            baseDate = this.baseDate.value,
            currentYear = moment(baseDate).year(),
            yearsRange = [currentYear, currentYear];

          if (GET_DEEP_PROPERTY(this.views.selection, ID) === TIME_PERIODS.YEAR) {
            yearsRange[0] = Math.floor(currentYear / 10) * 10;
            yearsRange[1] = yearsRange[0] + 9;
          }

          return `${yearsRange[0]} - ${yearsRange[1]}`;
        },
        [STEP]: [10, TIME_PERIODS.YEAR]
      },
    ])
    .select(TIME_PERIODS.DAY);

  private _getHeaderLabel = () => {
    const
      {model} = this,
      {settings, currentValue} = model,
      {placeholder, format} = settings;

    return IS_NULL(currentValue) ? <i className={`opacity-7`}>{placeholder}</i> : moment(currentValue).format(format);
  };

  private _toggle = e => {
    !this.model.isDisabled.value && this.model.isOpened.toggleValue();
  };

  componentDidMount(): void {
    const
      {model, views, baseDate} = this,
      {isOpened} = model;

    this.registerSubscriptions(
      isOpened.getSubscription(
        state => {
          if (state) {
            views.select(TIME_PERIODS.DAY);
            baseDate.setValue(AS_TIMESTAMP(START_OF(model.currentValue, TIME_PERIODS.DAY)));
          }
        }
      )
    );
  }

  render () {
    const
      {model, views, props, baseDate} = this,
      {className} = props,
      {isDisabled, isOpened} = model;

    return <div className={`
        ${ROOT}
        ${isOpened.value ? IS_OPENED : EMPTY_STRING}
        ${isDisabled.value ? IS_DISABLED : EMPTY_STRING}
        ${className || EMPTY_STRING}
      `}>
      <div className={`${HEADER} fc-base-input`} onClick={e => this._toggle(e)}>
        <div className={`${INPUT_LABEL} display-inline-flex padding-h-15`}>{this._getHeaderLabel()}</div>
        <div className={`${INPUT_ICON} display-inline-flex align-center`}>
          <IoMdCalendar className={`fc-input-icon`} />
        </div>
      </div>
      <FC_DropdownBody  className={`${BODY_BASE}`} isOpened={isOpened} children={
        <FC_DateSelectorBody model={model} views={views} baseDate={baseDate} />
      } />
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}

