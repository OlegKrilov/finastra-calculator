import {NumberObservable} from "../../Core/Observables/NumberObservable";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {computed} from "mobx";
import moment from "moment";
import {AS_TIMESTAMP, START_OF} from "../../Constatns/FC_Helpers";
import {TIME_PERIODS} from "../../Constatns/FC_Constants";
import {FC_AbstractInput} from "../FC_AbstractInput/FC_AbstractInput";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {IS_NULL} from "../../Core/Helpers/Helpers.misc";

export interface I_FC_DateSelectorSettings {
  placeholder?: string,
  format?: string
}

export class FC_DateSelectorModel extends FC_AbstractInput {

  public readonly settings: I_FC_DateSelectorSettings;

  public readonly core: NumberObservable;

  public readonly isOpened: SwitchObservable = new SwitchObservable();

  public toggleOpened = (state: boolean | null = null): FC_DateSelectorModel => {
    this.isOpened.toggleValue(state);
    return this;
  };

  public setValue = (newDate: any) => {
    this.core.setValue(AS_TIMESTAMP(START_OF(newDate, TIME_PERIODS.DAY)));
    return this;
  };

  public clearValue = () => {
    this.core.resetValue();
    return this;
  };

  public getDefaultSettings = (): I_FC_DateSelectorSettings => ({
    placeholder: 'Select Date',
    format: 'DD MMMM YYYY'
  });

  @computed
  public get currentValue () {
    return this.core.value || null;
  }

  constructor (customSettings: I_FC_DateSelectorSettings = {}, defaultValue: number | undefined = undefined) {
    super(defaultValue);
    this.core = new NumberObservable(this.defaultValue);
    this.settings = Object.assign(this.getDefaultSettings(), customSettings);
  }

}

