import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {FC_DateSelectorModel} from "./FC_DateSelector.model";
import {observer} from "mobx-react";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {NumberObservable} from "../../Core/Observables/NumberObservable";
import {CHECK_AND_CALL, GET_DEEP_PROPERTY, IS_NOT_NULL, IS_NULL} from "../../Core/Helpers/Helpers.misc";
import {
  ACTION,
  CLASSNAME,
  DATUM,
  ID,
  LABEL,
  STEP,
  X_VAL,
  Y_VAL
} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EMPTY_STRING, IS_ACTIVE} from "../../Core/Constants/ViewClasses.cnst";
import {IoIosArrowBack, IoIosArrowForward} from "react-icons/all";
import moment from "moment";
import {TIME_PERIODS} from "../../Constatns/FC_Constants";
import {AS_TIMESTAMP, START_OF} from "../../Constatns/FC_Helpers";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";

const
  ROOT = `fc-date-selector-body`,
  NAV_PANEL = `${ROOT}-nav-panel`,
  NAV_BUTTON = `${NAV_PANEL}-button`,
  NAV_ARROW = `${NAV_PANEL}-arrow`,

  DAY_SELECTOR = `${ROOT}-day-selector`,
  MONTH_SELECTOR = `${ROOT}-month-selector`,
  YEAR_SELECTOR = `${ROOT}-year-selector`,

  WEEKDAY_TILE = `${ROOT}-weekday-tile`,
  TILES = `${ROOT}-tiles`,
  TILE = `${ROOT}-tile`,
  TILE_INNER = `${TILE}-inner`,
  TILE_PLACEHOLDER = `${TILE}-placeholder`;

@observer
export class FC_DateSelectorBody extends AbstractComponent {

  private readonly model: FC_DateSelectorModel;

  private readonly views: AbstractCollection;

  private readonly baseDate: NumberObservable;

  private readonly tiles: AbstractCollection = new AbstractCollection();

  private _getNavLabel = () => {
    const {selection} = this.views;
    return selection ? CHECK_AND_CALL(selection[LABEL]) : EMPTY_STRING;
  };

  private _getWeekdayTiles = () => ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'].map((d, i) =>
    <div className={`${WEEKDAY_TILE} display-inline-flex align-center`} key={i}>
      <b>{d}</b>
    </div>
  );

  private _toggleTab = () => {
    const {selection} = this.views;
    selection && CHECK_AND_CALL(selection[ACTION]);
  };

  private _switchBaseDate = (direction) => {
    const
      {baseDate, views} = this,
      nextStep = GET_DEEP_PROPERTY(views.selection, STEP);

    nextStep && baseDate.setValue(
      AS_TIMESTAMP(START_OF(moment(baseDate.value).add(nextStep[0] * direction, nextStep[1]), TIME_PERIODS.DAY))
    );
  };

  private _onTileClick = (d) => {
    const
      {model, views, baseDate} = this,
      enabledView = GET_DEEP_PROPERTY(views.selection, ID),
      daySelectorEnabled = enabledView === TIME_PERIODS.DAY,
      monthSelectorEnabled = enabledView === TIME_PERIODS.MONTH,
      yearSelectorEnabled = enabledView === TIME_PERIODS.YEAR;

    if (daySelectorEnabled) {
      let selectedTimestamp = AS_TIMESTAMP(moment(d[DATUM]));
      if (selectedTimestamp !== model.currentValue) {
        model.setValue(selectedTimestamp);
        model.isOpened.toggleValue(false);
      }
    }

    else if (monthSelectorEnabled || yearSelectorEnabled) {
      let selectedTimestamp = AS_TIMESTAMP(moment(d[DATUM]));
      if (baseDate.value !== selectedTimestamp) {
        baseDate.setValue(selectedTimestamp);
        views.select(monthSelectorEnabled ? TIME_PERIODS.DAY : TIME_PERIODS.MONTH);
      }
    }
  };

  componentDidMount(): void {
    const
      {views, baseDate, tiles} = this;

    const _refreshView = () => {
      const
        selectedPeriod = GET_DEEP_PROPERTY(views.selection, ID),
        baseTimestamp = baseDate.value;

      if (selectedPeriod === TIME_PERIODS.DAY) {
        let
          fromMoment = START_OF(baseTimestamp, TIME_PERIODS.MONTH),
          offset = fromMoment.weekday();

        tiles
          .replaceItems(d3.range(offset).map(i => ({
            [DATUM]: null,
            [LABEL]: EMPTY_STRING,
            [CLASSNAME]: 'borderless'
          })))
          .addItems(d3.range(fromMoment.daysInMonth()).map(i => {
            let day = moment(fromMoment).add(i, TIME_PERIODS.DAY);
            return {
              [DATUM]: day,
              [LABEL]: day.date()
            }
          }));
      }

      else if (selectedPeriod === TIME_PERIODS.MONTH) {
        let
          fromMoment = START_OF(baseTimestamp, TIME_PERIODS.YEAR);

        tiles.replaceItems(d3.range(12).map(i => {
          let month = moment(fromMoment).add(i, TIME_PERIODS.MONTH);
          return {
            [DATUM]: month,
            [LABEL]: month.format('MMM')
          };
        }));
      }

      else if (selectedPeriod === TIME_PERIODS.YEAR) {
        let
          currentYear = moment(baseTimestamp).year(),
          fromMoment = moment(Math.floor(currentYear / 10) * 10, 'YYYY'),
          yearBefore = fromMoment.year() - 1,
          yearAfter = fromMoment.year() + 10;

        tiles
          .replaceItems({
            [DATUM]: null,
            [LABEL]: yearBefore
          })
          .addItems(d3.range(10).map(i => {
            let year = moment(fromMoment).add(i, TIME_PERIODS.YEAR);
            return {
              [DATUM]: year,
              [LABEL]: year.format('YYYY')
            }
          }))
          .addItems({
            [DATUM]: null,
            [LABEL]: yearAfter
          });
      }
    };

    this.registerSubscriptions(
      new AbstractObserver(
        () => `${GET_DEEP_PROPERTY(views.selection, ID)}_${baseDate.value}`
      ).getSubscription(
        () => _refreshView()
      )
    );
  }

  render () {
    const
      {model, views, baseDate, tiles} = this,
      enabledView = GET_DEEP_PROPERTY(views.selection, ID),
      daySelectorEnabled = enabledView === TIME_PERIODS.DAY,
      monthSelectorEnabled = enabledView === TIME_PERIODS.MONTH,
      yearSelectorEnabled = enabledView === TIME_PERIODS.YEAR;

    return <div className={`${ROOT}`}>

      <div className={`${NAV_PANEL} display-flex align-center pos-rel`}>
        <div className={`
           ${NAV_BUTTON} 
           ${yearSelectorEnabled ? EMPTY_STRING : IS_ACTIVE}
           display-flex align-center margin-top-10 padding-h-15
         `} onClick={() => !yearSelectorEnabled && this._toggleTab()}>{
          this._getNavLabel()
        }</div>
        <div className={`${NAV_ARROW} pos-abs top-10 left-10 display-flex align-center fc-color-blue`}
             onClick={() => this._switchBaseDate(-1)}>
          <IoIosArrowBack/>
        </div>
        <div className={`${NAV_ARROW} pos-abs top-10 right-10 display-flex align-center fc-color-blue`}
             onClick={() => this._switchBaseDate(1)}>
          <IoIosArrowForward/>
        </div>
      </div>

      <div className={`
          ${daySelectorEnabled ? DAY_SELECTOR : EMPTY_STRING}
          ${monthSelectorEnabled ? MONTH_SELECTOR : EMPTY_STRING}
          ${yearSelectorEnabled ? YEAR_SELECTOR : EMPTY_STRING}
          padding-10 font-size-12
      `}>
        {daySelectorEnabled && this._getWeekdayTiles()}
        <div className={`${TILES}`}>{tiles.items.map((d, i) =>
          <div className={`${TILE} ${d[CLASSNAME] || EMPTY_STRING} display-inline-flex`} key={i}>
            <div className={`
              ${IS_NULL(d[DATUM]) ? TILE_PLACEHOLDER : TILE_INNER} 
              display-flex align-center size-cover padding-3
            `} onClick={() => this._onTileClick(d)}>{d[LABEL]}</div>
          </div>
        )}</div>
      </div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
    this.views = props.views;
    this.baseDate = props.baseDate;
  }
}

