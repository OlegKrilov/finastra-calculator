import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {EMPTY_STRING, IS_OPENED} from "../../Core/Constants/ViewClasses.cnst";

const
  ROOT = 'fc-loader',
  CONTENT = `${ROOT}-content`,
  LABEL = `${ROOT}-label`,
  SPINNER = `${ROOT}-spinner`,
  CARET = `${SPINNER}-caret`;

@observer
export class FC_Loader extends AbstractComponent {

  private readonly isOpened: SwitchObservable;

  render () {
    const
      {isOpened, props} = this,
      {children, className} = props;

    return <div className={`${ROOT} 
        ${className || EMPTY_STRING} 
        ${isOpened.value ? IS_OPENED : EMPTY_STRING}
        pos-fix top-0 left-0 size-cover align-center
      `} onClick={() => isOpened.toggleValue(false)}>
      <div className={`${CONTENT}`}>
        <div className={`${LABEL} text-left`}>{children || <h2>LOADING</h2>}</div>
        <div className={`${SPINNER} pos-rel`}>
          <div className={`${CARET} pos-abs top-0`} />
          <div className={`${CARET} pos-abs top-0`} />
          <div className={`${CARET} pos-abs top-0`} />
        </div>
      </div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.isOpened = props.isOpened;
  }
}

