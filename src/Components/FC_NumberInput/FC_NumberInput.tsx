import * as d3 from 'd3';
import {Selection} from 'd3-selection';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {FC_NumberInputModel} from "./FC_NumberInput.model";
import {EMPTY_STRING, HAS_ERROR, IS_DISABLED, IS_FOCUSED} from "../../Core/Constants/ViewClasses.cnst";
import {observer} from "mobx-react";
import {CHECK_AND_CALL, GET_DEEP_PROPERTY, IS_FUNCTION, IS_NOT_NULL} from "../../Core/Helpers/Helpers.misc";
import {VALUE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractTimer} from "../../Core/Observers/AbstractTimer";

const
  ROOT = 'fc-number-input',
  INPUT_PLACEHOLDER = `${ROOT}-placeholder`,
  INPUT_CORE = `${ROOT}-core`,
  INPUT_LABEL = `${ROOT}-label`,
  INPUT_ICON = `${ROOT}-icon`,
  INFO_BUBBLE = `${ROOT}-info-bubble`;

@observer
export class FC_NumberInput extends AbstractComponent {

  private readonly model: FC_NumberInputModel;

  private readonly ref: any = React.createRef();

  private readonly errorTimer: AbstractTimer = new AbstractTimer({
    delay: 2000
  });

  private readonly elements: any = {};

  private _onChange = (e) => {
    let
      {model} = this,
      {target} = e,
      {validators} = model.settings,
      val = target.value || 0;

    if (model.checkValidity(val)) `${target.value}` !== `${model.currentValue}` && model.setValue(val);
    else target.value = model.currentValue;
  };

  private _onFocus = (e) => {
    const
      {model, elements} = this,
      {isFocused} = model;

    if (!isFocused.value) {
      let inputCoreNode = elements.inputCore.node();
      isFocused.toggleValue(true);
      inputCoreNode.setSelectionRange(0, inputCoreNode.value.length);
    }
  };

  private _onBlur = (e) => {
    const {isFocused} = this.model;
    isFocused.value && isFocused.setValue(false);
  };

  private _getFormattedLabel = () => {
    const
      {model} = this,
      {settings} = model,
      {getFormattedLabel} = settings;

    return IS_FUNCTION(getFormattedLabel) ?
      CHECK_AND_CALL(getFormattedLabel, model.currentValue) : getFormattedLabel;
  };

  private _getIcon = () => {
    const
      {model} = this,
      {settings} = model,
      {getIcon} = settings;

    return IS_FUNCTION(getIcon) ? CHECK_AND_CALL(getIcon) : getIcon;
  };

  private _getErrorMessage = () => {
    return GET_DEEP_PROPERTY(this.model.error, VALUE, 'message') || EMPTY_STRING;
  };

  componentDidMount(): void {
    const
      {model, ref, elements, errorTimer} = this,
      {isFocused, isDisabled} = model,
      root: Selection<any, any, any, any> = d3.select(ref.current),
      inputCore = root.select(`.${INPUT_CORE}`);

    Object.assign(elements, {root, inputCore});

    this.registerSubscriptions(

      isFocused.getSubscription(
        state => {
          if (isDisabled.value) isFocused.setValue(false);
          else {
            let inputCoreNode = inputCore.node();

            if (state) {
              inputCoreNode.focus();
              inputCoreNode.value = model.currentValue;
            }
            inputCoreNode[state ? 'focus' : 'blur']();
          }
        }
      ),

      model.core.getSubscription(
        () => {
          let
            inputCoreNode = inputCore.node(),
            val = inputCoreNode.value;

          `${val}` !== `${model.currentValue}` && (inputCoreNode.value = model.currentValue);
        }
      ),

      model.error.getSubscription(
        () => model.hasError && errorTimer.run(() => model.clearError())
      )
    );
  }

  componentWillUnmount(): void {
    super.componentWillUnmount();
    this.errorTimer.stop();
  }

  render () {
    const
      {model, ref, props} = this,
      {className} = props,
      {isDisabled, isFocused, hasError} = model;

    return <label className={`
          ${ROOT} fc-base-input display-flex align-left margin-0 pos-rel
          ${className || EMPTY_STRING}
          ${hasError ? HAS_ERROR : EMPTY_STRING}
          ${isDisabled.value ? IS_DISABLED : EMPTY_STRING}
          ${isFocused.value ? IS_FOCUSED : EMPTY_STRING}
      `} ref={ref}>
      <input className={`${INPUT_CORE} display-block padding-h-15`}
             onFocus={e => this._onFocus(e)}
             onBlur={e => this._onBlur(e)}
             onChange={e => this._onChange(e)} />
      <div className={`${INPUT_LABEL} pos-abs top-0 left-0 padding-h-15`}>{this._getFormattedLabel()}</div>
      <div className={`${INPUT_ICON} display-flex align-center fc-input-icon`}>{this._getIcon()}</div>
      <div className={`${INFO_BUBBLE} pos-abs padding-h-12 padding-v-6`}>{this._getErrorMessage()}</div>
    </label>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}

