import React from "react";
import {NumberObservable} from "../../Core/Observables/NumberObservable";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";
import {computed, observable} from "mobx";
import {FC_AbstractInput} from "../FC_AbstractInput/FC_AbstractInput";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {AS_ARRAY, CHECK_AND_CALL, IS_NOT_NULL, IS_NULL} from "../../Core/Helpers/Helpers.misc";
import {IS_VALID_NUMBER} from "../../Constatns/FC_Validators";

export interface I_FC_NumberInputSettings {
  placeholder?: string,
  getFormattedLabel?: any,
  getIcon?: any,
  validators?: any | null
}

export class FC_NumberInputModel extends FC_AbstractInput {

  public readonly settings: I_FC_NumberInputSettings;

  public readonly core: NumberObservable;

  public readonly onChange: AbstractEventObserver = new AbstractEventObserver('change');

  public readonly onFocus: AbstractEventObserver = new AbstractEventObserver('change');

  public readonly onBlur: AbstractEventObserver = new AbstractEventObserver('change');

  public setValue = (val: number = 0): FC_NumberInputModel => {
    this.checkValidity(val) && this.core.setValue(val);
    return this;
  };

  public clearValue = (): FC_NumberInputModel => {
    this.core.resetValue();
    return this;
  };

  public getDefaultSettings = (): I_FC_NumberInputSettings => ({
    placeholder: 'Number',
    getFormattedLabel: () => this.currentValue,
    getIcon: () => <i>#</i>,
    validators: null
  });

  public checkValidity = (val): boolean => {
    const
      {core, settings} = this,
      {validators} = settings;

    if (IS_NOT_NULL(validators)) return AS_ARRAY(validators).every(validator => CHECK_AND_CALL(validator, val));
    else return true;
  };

  @computed
  public get currentValue () {
    return this.core.value || 0;
  }

  constructor (customSettings: I_FC_NumberInputSettings = {}, defaultValue: number = 0) {
    super(defaultValue);
    this.core = new NumberObservable(this.defaultValue);
    this.settings = Object.assign(this.getDefaultSettings(), customSettings);
    this.settings.validators = IS_NULL(this.settings.validators) ? [] : AS_ARRAY(this.settings.validators);
    AS_ARRAY(this.settings.validators).unshift(IS_VALID_NUMBER(
      () => this.core.setValue(this.core.value)
    ));
  }
}