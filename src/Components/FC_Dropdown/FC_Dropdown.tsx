import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {FC_DropdownModel, I_FC_DropdownItem} from "./FC_Dropdown.model";
import {EMPTY_STRING, HAS_SELECTION, IS_DISABLED, IS_OPENED, IS_SELECTED} from "../../Core/Constants/ViewClasses.cnst";
import {FC_DropdownBody} from "../FC_DropdownBody/FC_DropdownBody";
import {CHECK_AND_CALL, CHECK_CLICK_TARGET, IS_FUNCTION, IS_STRING} from "../../Core/Helpers/Helpers.misc";
import {IoMdArrowDropdown} from "react-icons/all";
import {_IS_SELECTED_} from "../../Core/Abstract/AbstractCollection";
import {ID, LABEL} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {GlobalEventsService} from "../../Services/GlobalEvents.service";

const
  ROOT = 'fc-dropdown',
  HEADER = `${ROOT}-header`,
  HEADER_LABEL = `${HEADER}-label`,
  HEADER_PLACEHOLDER = `${HEADER}-placeholder`,
  HEADER_ICON = `${HEADER}-icon`,
  HIDDEN_INPUT = `${ROOT}-hidden-input`,
  LIST = `${ROOT}-list`,
  ITEMS = `${LIST}-items`,
  ITEM = `${LIST}-item`,
  NO_DATA = `${ROOT}-no-data`;

@observer
export class FC_Dropdown extends AbstractComponent {

  private readonly model: FC_DropdownModel;

  private readonly globalEventsService: GlobalEventsService;

  private _toggle = () => !this.model.isDisabled.value && this.model.isOpened.toggleValue();

  private _toggleItem = (d: I_FC_DropdownItem) => this.model.items.select(d[ID]);

  private _getHeaderLabel = () => {
    const
      {model} = this,
      {settings} = model,
      {getHeaderLabel, placeholder} = settings,
      hasSelection = !!model.selection;

    return hasSelection ?
      (IS_FUNCTION(getHeaderLabel) ? CHECK_AND_CALL(getHeaderLabel, model.items.selection) : getHeaderLabel) :
      <i className={`opacity-7`}>{placeholder}</i>;
  };

  private _getListItemLabel = (d: I_FC_DropdownItem) => {
    const {getListItemLabel} = this.model.settings;
    return IS_FUNCTION(getListItemLabel) ? CHECK_AND_CALL(getListItemLabel, d) : d[LABEL];
  };

  componentDidMount(): void {
    const
      {model} = this,
      {isOpened, isDisabled, items, onChange} = model;

    this.registerSubscriptions(
      isDisabled.getSubscription(
        state => state && isOpened.setValue(false)
      ),
      items.subscribeOnSelectionChange(
        (selection) => {
          isOpened.setValue(false);
          onChange.next('wasChanged', selection);
        }
      )
    );
  }

  render () {

    const
      {model, props} = this,
      {className} = props,
      {items} = model.items,
      hasSelection = !!model.selection,
      isDisabled = model.isDisabled.value,
      isOpened = !isDisabled && model.isOpened.value;

    return <div className={`${ROOT}
        ${isDisabled ? IS_DISABLED : EMPTY_STRING}
        ${isOpened ? IS_OPENED : EMPTY_STRING}
        ${hasSelection ? HAS_SELECTION : EMPTY_STRING}
        ${className || EMPTY_STRING}
      `}>
      <input type={`text`} className={`${HIDDEN_INPUT}`}/>
      <div className={`${HEADER} fc-base-input`} onClick={() => this._toggle()}>
        <div className={`${HEADER_LABEL} display-inline-flex padding-h-15`}>{this._getHeaderLabel()}</div>
        <div className={`${HEADER_ICON} display-inline-flex align-center`}>
          <IoMdArrowDropdown className={`fc-input-icon font-size-16`} />
        </div>
      </div>
      <FC_DropdownBody className={LIST} isOpened={model.isOpened} children={
        <div className={`${ITEMS} padding-v-10`}>
          {items.length && items.map((d, i) =>
            <div className={`
                  ${ITEM} 
                  ${CHECK_AND_CALL(d[_IS_SELECTED_]) ? IS_SELECTED : EMPTY_STRING} 
                  padding-h-15 padding-v-7
               `}
               onClick={() => this._toggleItem(d)}
               key={i}>{this._getListItemLabel(d)}
            </div>
          )}
          {!items.length && <div className={`${NO_DATA}`}>{model.settings.noDataLabel}</div>}
        </div>
      }/>
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}

