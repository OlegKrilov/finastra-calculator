import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {ID, LABEL} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AS_ARRAY, GET_DEEP_PROPERTY, GET_RANDOM_ID, IS_OBJECT, IS_STRING} from "../../Core/Helpers/Helpers.misc";
import {computed} from "mobx";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";
import {FC_AbstractInput} from "../FC_AbstractInput/FC_AbstractInput";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

export interface I_FC_DropdownSettings {
  noDataLabel?: string,
  placeholder?: string,
  getHeaderLabel?: any,
  getListItemLabel?: any
}

export interface I_FC_DropdownItem {
  [ID]: string,
  [LABEL]: string
}

export class FC_DropdownModel extends FC_AbstractInput {

  public readonly settings: I_FC_DropdownSettings;

  public readonly isOpened: SwitchObservable = new SwitchObservable();

  public readonly items: AbstractCollection = new AbstractCollection(ID);

  public readonly onChange: AbstractEventObserver = new AbstractEventObserver('change');

  public toggleOpened = (state: boolean | null = null): FC_DropdownModel => {
    this.isOpened.toggleValue(state);
    return this;
  };

  public addItems = (items: any): FC_DropdownModel => {
    this.items.addItems(AS_ARRAY(items).reduce((agg, d) => {
      if (IS_STRING(d)) agg.push({
        [ID]: d,
        [LABEL]: d
      });
      else if (IS_OBJECT(d)) {
        d[ID] = d[ID] || d[LABEL] || GET_RANDOM_ID();
        agg.push(d);
      }
      return agg;
    }, []));
    return this;
  };

  public clearItems = (): FC_DropdownModel => {
    this.items.clearItems();
    return this;
  };

  public select = (item: any): FC_DropdownModel => {
    this.items.select(item);
    return this;
  };

  public clearValue = (): FC_DropdownModel => {
    this.items.clearSelection();
    this.hasDefaultValue && this.items.select(this.defaultValue);
    return this;
  };

  public getDefaultSettings = (): I_FC_DropdownSettings => ({
    noDataLabel: 'No Data',
    placeholder: 'Select',
    getHeaderLabel: () => GET_DEEP_PROPERTY(this.items.selection, LABEL),
    getListItemLabel: (d) => GET_DEEP_PROPERTY(d, LABEL)
  } as I_FC_DropdownSettings);

  @computed
  public get selection () {
    return GET_DEEP_PROPERTY(this.items.selection, LABEL) || null;
  }

  @computed
  public get currentValue () {
    return this.items.selection;
  }

  constructor (customSettings: I_FC_DropdownSettings = {}, defaultValue: string | undefined = undefined) {
    super(defaultValue);
    this.settings = Object.assign(this.getDefaultSettings(), customSettings);
  }

}

