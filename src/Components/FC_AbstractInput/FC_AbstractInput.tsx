import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {IS_NOT_NULL} from "../../Core/Helpers/Helpers.misc";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {computed} from "mobx";

export class FC_AbstractInput {

  public readonly defaultValue: any;

  public readonly isFocused: SwitchObservable = new SwitchObservable();

  public readonly isDisabled: SwitchObservable = new SwitchObservable();

  public readonly error: StructureObservable = new StructureObservable();

  public toggleFocused = (state: boolean | null = null): this => {
    this.isFocused.toggleValue(state);
    return this;
  };

  public toggleDisabled = (state: boolean | null = null): this => {
    this.isDisabled.toggleValue(state);
    return this;
  };

  public setError = (error: string | any): this => {
    this.error.setValue({message: error});
    return this;
  };

  public clearError = (): this => {
    this.error.resetValue();
    return this;
  };

  @computed
  public get hasDefaultValue () {
    return IS_NOT_NULL(this.defaultValue);
  }

  @computed
  public get hasError () {
    return !this.error.isEmpty;
  }

  constructor (defaultValue: any = undefined) {
    this.defaultValue = defaultValue;
  }
}