import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {FC_ButtonModel} from "./FC_Button.model";
import {AbstractSubscription} from "../../Core/Abstract/AbstractSubscription";
import {CHECK_AND_CALL, IS_FUNCTION} from "../../Core/Helpers/Helpers.misc";
import {VALUE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EMPTY_STRING, IS_DISABLED} from "../../Core/Constants/ViewClasses.cnst";

const
  ROOT = 'fc-button';

@observer
export class FC_Button extends AbstractComponent {

  private readonly model: FC_ButtonModel;

  private onClick = e => !this.model.isDisabled.value && this.model.onClick.next(e);

  componentDidMount(): void {
    const
      {props, model} = this;

    if (IS_FUNCTION(props.onClick)) this.registerSubscriptions(
      model.onClick.getSubscription(e => CHECK_AND_CALL(props.onClick, e))
    );
  }

  render () {
    const
      {props, model} = this,
      {className, children} = props,
      isDisabled = model.isDisabled[VALUE];

    return <div className={`
      ${ROOT} 
      ${isDisabled ? IS_DISABLED : EMPTY_STRING} 
      ${className || EMPTY_STRING}
      display-inline-flex padding-h-15 fc-base-input`
    } onClick={e => this.onClick(e)}>{children || EMPTY_STRING}</div>;
  }



  constructor (props) {
    super(props);
    this.model = props.model instanceof FC_ButtonModel ? props.model : new FC_ButtonModel();

  }

}

