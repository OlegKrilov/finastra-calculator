import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";

export class FC_ButtonModel {

  public onClick: AbstractEventObserver = new AbstractEventObserver('click');

  public isDisabled: SwitchObservable = new SwitchObservable();

  public toggleDisabled = (state: boolean | null = null): FC_ButtonModel => {
    this.isDisabled.setValue(state);
    return this;
  };

}

