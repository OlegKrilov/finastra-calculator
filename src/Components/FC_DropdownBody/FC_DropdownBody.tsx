import * as d3 from 'd3';
import React from "react";
import {Selection} from 'd3-selection';
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {EMPTY_STRING, IS_OPENED, IS_REVERSED} from "../../Core/Constants/ViewClasses.cnst";
import {GlobalEventsService} from "../../Services/GlobalEvents.service";
import {AbstractSubscription} from "../../Core/Abstract/AbstractSubscription";
import {CHECK_CLICK_TARGET} from "../../Core/Helpers/Helpers.misc";
import {StringObservable} from "../../Core/Observables/StringObservable";

const
  ROOT = 'fc-dropdown-body',
  CONTENT = `${ROOT}-content`,
  CONTENT_STRAIGHT = `${CONTENT}-straight`,
  CONTENT_REVERSED = `${CONTENT}-reversed`;

@inject('globalEventsService')
@observer
export class FC_DropdownBody extends AbstractComponent {

  private readonly globalEventsService: GlobalEventsService;

  private readonly ref: any = React.createRef();

  private readonly isOpened: SwitchObservable;

  private readonly _className: StringObservable = new StringObservable();

  componentDidMount(): void {
    const
      {ref, isOpened, _className, globalEventsService} = this,
      root: Selection<any, any, any, any> = d3.select(ref.current);

    this.registerSubscriptions(
      globalEventsService.onMouseDown.getSubscription(
        (e) =>
          isOpened.value &&
          !CHECK_CLICK_TARGET(root.node(), e.originalEvent.target) &&
          isOpened.toggleValue(false)
      ),

      isOpened.getSubscription(
        state => _className.setValue(`
          ${window.innerHeight * .5 <  root.node().getBoundingClientRect().top ? IS_REVERSED : EMPTY_STRING}
          ${state ? IS_OPENED : EMPTY_STRING}
        `)
      )
    );
  }

  render () {
    const
      {props, _className, ref} = this,
      {className, children} = props;

    return <div className={`
        ${ROOT}
        ${_className.value}
        ${className ? className : EMPTY_STRING}
        pos-rel`} ref={ref}>
      <div className={`${CONTENT} ${CONTENT_STRAIGHT} pos-abs left-0 full-width`}>{children || EMPTY_STRING}</div>
      <div className={`${CONTENT} ${CONTENT_REVERSED} pos-abs left-0 full-width`}>{children || EMPTY_STRING}</div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.globalEventsService = props.globalEventsService;
    this.isOpened = props.isOpened;
  }

}

