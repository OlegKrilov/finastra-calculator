import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {COLOR, ID, VALUE} from "../../Core/Constants/PropertiesAndAttributes.cnst";

export interface I_FC_RoundChartSettings {
  radius?: number,
  valueKey?: string,
  colorKey?: string | any
}

export class FC_RoundChartModel {

  public readonly settings: any;

  public readonly slices: AbstractCollection = new AbstractCollection(ID);

  public refreshItems (data: any = []): FC_RoundChartModel {
    this.slices.replaceItems(data);
    return this;
  }

  public getDefaultSettings = (): I_FC_RoundChartSettings => ({
    radius: 0,
    valueKey: VALUE,
    colorKey: COLOR
  });

  constructor (customSettings: I_FC_RoundChartSettings = {}) {
    this.settings = Object.assign(this.getDefaultSettings(), customSettings);
  }

}