import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {FC_RoundChartModel, I_FC_RoundChartSettings} from "./FC_RoundChart.model";
import {Selection} from 'd3-selection';
import {observer} from "mobx-react";
import {ResizeObserver} from "../../Core/Observers/ResizeObserver";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {DATA, ELEMENT, HEIGHT, TRANSFORM, TRANSLATE, WIDTH} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {GET_DEEP_PROPERTY} from "../../Core/Helpers/Helpers.misc";

const
  ROOT = 'fc-round-chart',
  CORE = `${ROOT}-core`,
  LAYOUT = `${ROOT}-layout`,
  SLICES = `${ROOT}-slices`,
  SLICE = `${ROOT}-slice`,
  SLICE_COLOR = `${SLICE}-color`,
  SLICE_INTERACTIVE = `${SLICE}-interactive`;

@observer
export class FC_RoundChart extends AbstractComponent {

  private readonly model: FC_RoundChartModel;

  private readonly ref: any = React.createRef();

  private resizeObserver: ResizeObserver;

  componentDidMount(): void {
    const
      {model} = this,
      root = d3.select(this.ref.current),
      core = root.select(`.${CORE}`),
      layout = core.select(`.${LAYOUT}`),
      slices = layout.select(`.${SLICES}`);

    this.resizeObserver = new ResizeObserver(root.node()).run();

    const
      _refreshView = () => {
        const
          {width, height} = this.resizeObserver.value,
          settings: I_FC_RoundChartSettings = this.model.settings,
          outerRadius = d3.min([width, height]) * .5,
          pie = d3.pie().sort(null).value(d => GET_DEEP_PROPERTY(d, settings.valueKey)),
          arc = d3.arc().innerRadius(outerRadius  * ((settings.radius as number) * 0.1)).outerRadius(outerRadius),
          data = pie(model.slices.items);

        core
          .style(WIDTH, () => `${width}px`)
          .style(HEIGHT, () => `${height}px`);

        layout
          .attr(TRANSFORM, () => `${TRANSLATE}(${width * .5}, ${height * .5})`);

        slices.selectAll(`.${SLICE}`).remove();

        slices
          .selectAll(`.${SLICE}`).data(data)
          .enter().append('g').classed(SLICE, true)
          .exit().remove();

        slices
          .selectAll(`.${SLICE}`).each(function (this, d, i) {
            d[ELEMENT] = d3.select(this);

            d[ELEMENT]
              .append('path').classed(SLICE_COLOR, true);

            d[ELEMENT]
              .append('path').classed(SLICE_INTERACTIVE, true);

            d[ELEMENT]
              .selectAll('path').attr('d', () => arc(d));

            d[ELEMENT]
              .select(`.${SLICE_COLOR}`)
              .attr('fill', () => GET_DEEP_PROPERTY(d, DATA, settings.colorKey));
            
            d[ELEMENT]
              .select(`.${SLICE_INTERACTIVE}`)
              .attr('fill', () => 'rgba(0,0,0,0)')
              .on('mouseover', () => d[ELEMENT]
                .select(`.${SLICE_INTERACTIVE}`)
                .transition().duration(500)
                .attr('fill', () => 'rgba(0,0,0,.3)')
              )
              .on('mouseout', () => d[ELEMENT]
                .select(`.${SLICE_INTERACTIVE}`)
                .transition().duration(500)
                .attr('fill', () => 'rgba(0,0,0,0)')
              )
          });
      };

    this.registerSubscriptions(
      new AbstractObserver(() => [model.slices.hash, this.resizeObserver.value]).getSubscription(
        () => _refreshView()
      )
    );
  }

  componentWillUnmount(): void {
    super.componentWillUnmount();
    this.resizeObserver && this.resizeObserver.stop();
  }

  render () {
    const
      {props} = this,
      {className} = props;

    return <div className={`${ROOT} ${className || EMPTY_STRING}`} ref={this.ref}>
      <svg className={`${CORE}`}>
        <g className={`${LAYOUT}`}>
          <g className={`${SLICES}`} />
        </g>
      </svg>
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}

