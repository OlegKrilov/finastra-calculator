import {StructureObservable} from '../Observables/StructureObservable';
import {AbstractTimer} from './AbstractTimer';
import {AbstractSubscription} from '../Abstract/AbstractSubscription';
import {computed} from 'mobx';
import {AnimationFrameObserver} from './AnimationFrameObserver';
import {CLIENT_HEIGHT, CLIENT_WIDTH, HEIGHT, WIDTH} from '../Constants/PropertiesAndAttributes.cnst';
import {CHECK_AND_CALL, IS_NULL} from '../Helpers/Helpers.misc';

export interface IBlockSize {
  [WIDTH]: number;
  [HEIGHT]: number;
}

export class ResizeObserver {

  private readonly _core: StructureObservable = new StructureObservable(<IBlockSize> {
    [WIDTH]: 0,
    [HEIGHT]: 0
  });

  private readonly _target: any = {};

  private readonly _animationFrameObserver: AnimationFrameObserver;

  private readonly _widthProp: string;

  private readonly _heightProp: string;

  private _animationFrameSubscription: AbstractSubscription;

  public run (): this {
    !this._animationFrameObserver.isRunning && this._animationFrameObserver.run();
    this._animationFrameSubscription = this._animationFrameObserver.getSubscription(
      () => this._core.setValue({
        [WIDTH]: this._target[this._widthProp],
        [HEIGHT]: this._target[this._heightProp]
      })
    );
    return this;
  }

  public stop (): this {
    this._animationFrameObserver.stop();
    this._animationFrameSubscription && this._animationFrameSubscription.unsubscribe();
    return this;
  }

  public getSubscription (fn: any, skipFirstCall: boolean = false) {
    return this._core.getSubscription(() => {
      let currentValue = this.value;
      this._animationFrameObserver.subscribeOnNext(() => {
        let newValue = this.value;
        currentValue[WIDTH] === newValue[WIDTH] &&
        currentValue[HEIGHT] === newValue[HEIGHT] &&
        CHECK_AND_CALL(fn, newValue);
      });
    }, skipFirstCall);
  }

  @computed
  public get value (): IBlockSize {
    return this._core.value;
  }

  @computed
  public get isValid (): boolean {
    return Object.values(this.value).every(val => !!val);
  }

  constructor (
    target: HTMLElement | Window | any,
    animationFrameObserver: AnimationFrameObserver | null = null,
    widthProp: string = CLIENT_WIDTH,
    heightProp: string = CLIENT_HEIGHT
  ) {
    this._target = target;
    this._animationFrameObserver = animationFrameObserver || new AnimationFrameObserver();
    this._widthProp = widthProp;
    this._heightProp = heightProp;
  }
}

