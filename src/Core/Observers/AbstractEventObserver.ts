import {StructureObservable} from '../Observables/StructureObservable';
import {NEXT_TICK} from "../Helpers/Helpers.misc";

export interface IAbstractEvent {
  type: string;
  timestamp: number;
  originalEvent: any;
  data: any;
}

export class AbstractEventObserver {

  private readonly _core: StructureObservable = new StructureObservable();

  public readonly type: string;

  public next (event: any, data: any = null) {
    this._core.setValue({
      type: this.type,
      timestamp: new Date().getTime(),
      originalEvent: event,
      data
    } as IAbstractEvent);

    NEXT_TICK().then(() => this._core.resetValue());
  }

  public getSubscription = (callback: any, skipFirstCall: boolean = false) => this._core.getSubscription(
    () => !this._core.isEmpty && callback(this._core.value),
    skipFirstCall
  );

  public subscribeOnNext (callback: any) {
    let subscription = this.getSubscription(
      () => {
        callback(this._core.value);
        subscription && subscription.unsubscribe();
      }
    );
    return subscription;
  }

  constructor (eventType: string = 'click') {
    this.type = eventType;
  }
}

