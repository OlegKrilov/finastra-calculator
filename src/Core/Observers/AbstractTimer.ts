import {CHECK_AND_CALL, IS_OBJECT} from '../Helpers/Helpers.misc';
import {SwitchObservable} from '../Observables/SwitchObservable';

export interface IAbstractTimerSettings {
  delay?: number;
  loop?: boolean;
  runImmediately?: boolean;
}

export class AbstractTimer {

  private static _getDefaultSettings = (): IAbstractTimerSettings => ({
    delay: 25,
    loop: false,
    runImmediately: false
  });

  private _timer: any = null;

  private _settings: IAbstractTimerSettings = AbstractTimer._getDefaultSettings();

  private _isEnabled: SwitchObservable = new SwitchObservable();

  private _dropTimeout (): this {
    this._timer && clearTimeout(this._timer);
    this._timer = null;
    return this;
  }

  public run (callback) {
    let
      {delay, loop, runImmediately} = this._settings,
      _run = () => {
        this._dropTimeout();
        this._isEnabled.value && (this._timer = setTimeout(() => {
          CHECK_AND_CALL(callback);
          loop && _run();
        }, delay));
      };

    this._isEnabled.setValue(true);
    runImmediately && CHECK_AND_CALL(callback);
    _run();
  }

  public stop (): this {
    this._isEnabled.setValue(false);
    return this;
  }

  public get isEnabled (): boolean {
    return this._isEnabled.value;
  }

  constructor (settings: IAbstractTimerSettings = {}) {
    Object.assign(this._settings, IS_OBJECT(settings) ? settings : {});
  }

}

