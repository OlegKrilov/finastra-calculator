import {NumberObservable} from '../Observables/NumberObservable';
import {AbstractSubscription} from '../Abstract/AbstractSubscription';
import {computed} from 'mobx';
import {CHECK_AND_CALL, IS_NOT_NULL} from '../Helpers/Helpers.misc';

export class AnimationFrameObserver {

  private _core: NumberObservable = new NumberObservable();

  private _frame: any = null;

  private _next (): this {
    this._core.setValue(new Date().getTime());
    return this;
  }

  public run (): this {
    const
      self = this,
      nextFrame = () => {
        self._frame = requestAnimationFrame(function () {
          self._next();
          nextFrame();
        });
      };

    this.isRunning && this.stop();
    nextFrame();
    return this;
  }

  public stop (): this {
    this._frame && cancelAnimationFrame(this._frame);
    this._frame = null;
    return this;
  }

  public getSubscription (fn: any, skipFirstCall: boolean = false): AbstractSubscription {
    let i = 0;
    return this._core.getSubscription(() => CHECK_AND_CALL(fn, i++), skipFirstCall);
  }

  public subscribeOnNext (fn) {
    return this._core.subscribeOnNext(fn);
  }

  @computed
  public get isRunning (): boolean {
    return IS_NOT_NULL(this._frame);
  }

  @computed
  public get tick (): number {
    return this._core.value;
  }
}

export default new AnimationFrameObserver();

