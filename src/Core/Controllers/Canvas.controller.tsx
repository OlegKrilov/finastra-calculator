import * as d3 from 'd3';
import {Selection} from 'd3-selection';
import {
  ANGLE,
  DELAY,
  DURATION, DX_VAL, DY_VAL, FILL_STYLE,
  HEIGHT,
  LEFT, LINE_WIDTH, R_VAL,
  ROTATE,
  SCALE, SHADOW, SHADOW_BLUR, SHADOW_COLOR, SHADOW_OFFSET_X, SHADOW_OFFSET_Y, STROKE_STYLE,
  TOP,
  TRANSFORM,
  TRANSLATE,
  WIDTH, X_VAL, Y_VAL
} from '../Constants/PropertiesAndAttributes.cnst';
import {StructureObservable} from '../Observables/StructureObservable';
import {AbstractEventObserver} from '../Observers/AbstractEventObserver';

export interface IAppCanvasDimensions {
  [WIDTH]?: number,
  [HEIGHT]?: number,
  [TOP]?: number,
  [LEFT]?: number,
  [SCALE]?: number,
  [ANGLE]?: number
}

export interface IAnimationSettings {
  [DURATION]?: number,
  [DELAY]?: number
}

export interface IShadowProps {
  [SHADOW_OFFSET_X]?: number,
  [SHADOW_OFFSET_Y]?: number,
  [SHADOW_COLOR]?: string,
  [SHADOW_BLUR]?: number
}

export class CanvasController {

  private readonly _canvas: Selection<HTMLCanvasElement, any, any, any>;

  private readonly _context: CanvasRenderingContext2D | null | any;

  private readonly _attributes: StructureObservable = new StructureObservable( {
    [WIDTH]: 0,
    [HEIGHT]: 0,
    [TOP]: 0,
    [LEFT]: 0,
    [SCALE]: 1,
    [ANGLE]: 0
  } as IAppCanvasDimensions);

  private readonly _animationAttributes: StructureObservable = new StructureObservable({
    [DURATION]: 0,
    [DELAY]: 0
  } as IAnimationSettings);

  public changeView (customAttributes: IAppCanvasDimensions = {}): this {
    const {_canvas, _attributes} = this;

    _attributes.setValue(customAttributes);

    let
      attributes: IAppCanvasDimensions = this._attributes.value;

    _canvas
      .attr(WIDTH, () => attributes[WIDTH])
      .attr(HEIGHT, () => attributes[HEIGHT]);

    return this;
  }

  public drawRect (rectData): this {
    const
  {_context} = this,
    useShadow = rectData[SHADOW],
    useStroke = rectData[STROKE_STYLE] || rectData[LINE_WIDTH],
    useFill = rectData[FILL_STYLE];

    _context.beginPath();
    _context.rect(rectData[X_VAL], rectData[Y_VAL], rectData[WIDTH], rectData[HEIGHT]);

    rectData[STROKE_STYLE] && this.setStrokeStyle(rectData[STROKE_STYLE]);
    rectData[LINE_WIDTH] && this.setLineWidth(rectData[LINE_WIDTH]);
    useShadow && this.setShadow(rectData[SHADOW]);
    useFill && this.setFillStyle(rectData[FILL_STYLE]);

    useStroke && _context.stroke();
    useFill && _context.fill();
    _context.closePath();

    return this;
  }

  public drawLine (lineData): this {
    const
    {_context} = this;

    _context.beginPath();

    _context.moveTo(lineData[X_VAL], lineData[Y_VAL]);
    _context.lineTo(lineData[DX_VAL], lineData[DY_VAL]);

    lineData[STROKE_STYLE] && this.setStrokeStyle(lineData[STROKE_STYLE]);
    lineData[LINE_WIDTH] && this.setLineWidth(lineData[LINE_WIDTH]);

    _context.stroke();
    _context.closePath();

    return this;
  }

  public drawCircle (circleData): this {
    const
    {_context} = this,
    useShadow = circleData[SHADOW],
    useStroke = circleData[STROKE_STYLE] || circleData[LINE_WIDTH],
    useFill = circleData[FILL_STYLE];

    _context.beginPath();

    _context.arc(circleData[X_VAL], circleData[Y_VAL], circleData[R_VAL], 0, Math.PI * 2);

    circleData[STROKE_STYLE] && this.setStrokeStyle(circleData[STROKE_STYLE]);
    circleData[LINE_WIDTH] && this.setLineWidth(circleData[LINE_WIDTH]);
    useShadow && this.setShadow(circleData[SHADOW]);
    useFill && this.setFillStyle(circleData[FILL_STYLE]);

    useStroke && _context.stroke();
    useFill && _context.fill();
    _context.closePath();

    return this;
  }

  public drawPoint (pointData): this {
    const {_context} = this;

    _context.beginPath();

    _context[LINE_WIDTH] = pointData[R_VAL];
    _context.moveTo(pointData[X_VAL], pointData[Y_VAL]);
    _context.lineTo(pointData[X_VAL], pointData[Y_VAL]);
    _context[STROKE_STYLE] = pointData[FILL_STYLE];

    _context.stroke();
    _context.closePath();

    return this;
  }

  public clear (): this {
    this._context.clearRect(0, 0, this.width, this.height);
    return this;
  }

  public setFillStyle (c: string = '#000'): this {
    this._context[FILL_STYLE] = c;
    return this;
  }

  public setStrokeStyle (c: string = '#000'): this {
    this._context[STROKE_STYLE] = c;
    return this;
  }

  public setLineWidth (w: number = 1): this {
    this._context[LINE_WIDTH] = w;
    return this;
  }

  public setShadow (s: IShadowProps = {}) {
    Object.keys(s).forEach(key => this._context[key] = s[key]);
    return this;
  }

  public get context ():CanvasRenderingContext2D {
    return this._context;
  }

  public get width (): number {
    return this._attributes.value[WIDTH];
  }

  public get height (): number {
    return this._attributes.value[HEIGHT];
  }

  public get rotate (): number {
    return this._attributes.value[ROTATE];
  }

  constructor (ref: HTMLCanvasElement) {
    this._canvas = d3.select(ref);
    this._context = ref.getContext('2d');
  }
}

