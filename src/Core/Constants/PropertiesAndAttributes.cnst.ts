export const

  /** Keys */
  ENTER_KEY = 13, WILDCARD = '*',

  /** Values & identifiers */
  ID = 'id', DB_ID = '_id', IS_OBSERVABLE = 'isObservable',
  INDEX = 'Routes.scss', SECURED = 'secured',
  NAME = 'name', VALUE = 'value', LABEL = 'label',
  DATE = 'date', DATA = 'data', DATUM = 'datum', CHILDREN = 'children',
  ELEMENT = 'element', SOURCE = 'source', LINK = 'link',
  PATH = 'path', PRIORITY = 'priority', TYPE = 'type', OPTIONS = 'options',
  CLASSNAME = 'className', EMAIL = 'email', PHONE = 'phone', PLACEHOLDER = 'placeholder',
  DESCRIPTION = 'description', COMPONENT = 'component',
  MULTISELECT = 'allowMultiselect', ACTION = 'action', KEYS = 'keys',
  AMPLITUDE = 'amplitude', FREQUENCY = 'frequency', COLLECTION = 'collection',
  CONTENT = 'content', CONTENT_TYPE = 'contentType', FORM_NAME = 'formName',
  TAGS = 'tags', ADMIN = 'admin', STEP = 'step', MODEL = 'model', MAX = 'max', MIN = 'min',

  /** Background animations */
  OPEN = 'open', SIDE = 'side', AXIS = 'axis',

  /** Parts */
  FOLDER = 'folder', FILE = 'file', ANCHOR = 'anchor', ICON = 'icon',
  HEADER = 'header', BODY = 'body', FOOTER = 'footer', LIST = 'LIST', FORM = 'form',

  /** Sizes, positions & mutations */
  CLIENT_WIDTH = 'clientWidth', CLIENT_HEIGHT = 'clientHeight',
  WIDTH = 'width', HEIGHT = 'height', CENTER = 'center',
  TOP = 'top', LEFT = 'left', RIGHT = 'right', BOTTOM = 'bottom',
  UP = 'up', DOWN = 'down',
  ROTATE = 'rotate', SCALE = 'scale', OPACITY = 'opacity',
  PERSPECTIVE = 'perspective',
  TRANSLATE_X = 'translateX', TRANSLATE_Y = 'translateY', TRANSLATE_Z = 'translateZ',
  ROTATE_X = 'rotateX', ROTATE_Y = 'rotateY', ROTATE_Z = 'rotateZ', ROTATE_3D = 'rotate3d',
  TRANSLATE = 'translate', TRANSFORM = 'transform', SKEW = 'skew', TRANSFORM_ORIGIN = 'transform-origin',
  ANGLE = 'angle', ALIGN = 'align', SIZE = 'size', WEIGHT = 'weight', FONT_SIZE = 'fontSize',

  /** Transition */
  DURATION = 'duration', DELAY = 'delay',

  /** Canvas properties*/
  FILL_STYLE = 'fillStyle', STROKE_STYLE = 'strokeStyle', LINE_WIDTH = 'lineWidth',
  SHADOW = 'shadow', SHADOW_OFFSET_X = 'shadowOffsetX', SHADOW_OFFSET_Y = 'shadowOffsetY',
  SHADOW_COLOR = 'shadowColor', SHADOW_BLUR = 'shadowBlur',
  X_VAL = 'x', Y_VAL = 'y', Z_VAL = 'z', R_VAL = 'radius',
  DX_VAL = 'dx', DY_VAL = 'dy', DZ_VAL = 'dz',
  CX_VAL = 'cx', CY_VAL = 'cy', CZ_VAL = 'cz',
  COLOR = 'color',

  /** CSS */
  BACKGROUND_SIZE = 'background-size', BACKGROUND_IMAGE = 'background-image', BACKGROUND_POSITION = 'background-position',
  COVER = 'cover', BLUR = 'blur', FILTER = 'filter', DISPLAY = 'display', BLOCK = 'block', NONE = 'none', FLEX = 'flex',

  /** Slide */
  TASK = 'task', IMAGE = 'image', TEXT = 'text',

  /** Colors */
  WHITE = '#fff',
  BLACK = '#000',
  TRANSPARENT = 'rgba(0,0,0,0)';

