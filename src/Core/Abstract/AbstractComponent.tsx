import React from 'react';
import {AS_ARRAY, CHECK_AND_CALL} from '../Helpers/Helpers.misc';
import {observer} from 'mobx-react';
import {AbstractSubscription} from './AbstractSubscription';

@observer
export class AbstractComponent extends React.Component <any, any> {

  protected subscriptions: AbstractSubscription[] = [];

  protected registerSubscriptions (...args): void {
    AS_ARRAY(args).forEach(s => this.subscriptions = this.subscriptions.concat(s));
  }

  protected clearSubscriptions (): void {
    this.subscriptions.forEach(s => s && s.unsubscribe());
  }

  componentWillUnmount(): void {
    this.clearSubscriptions();
  }
}