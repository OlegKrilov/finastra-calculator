import {CHECK_AND_CALL} from '../Helpers/Helpers.misc';
import {computed, IComputedValue} from 'mobx';

export class AbstractSubscription {

  private static _index: number = 0;

  public readonly id: string = `AbstractSubscription#${AbstractSubscription._index++}`;

  private readonly _ra: any;

  private readonly _cs: any;

  public readonly getObserver: any;

  public callReaction (): this {
    CHECK_AND_CALL(this._ra);
    return this;
  }

  public unsubscribe (): this {
    CHECK_AND_CALL(this._cs);
    return this;
  }

  constructor (reaction: any, observer: IComputedValue<any>) {
    this.getObserver = () => observer;
    this._ra = () => CHECK_AND_CALL(reaction, observer.get());
    this._cs = observer.observe(() => this.callReaction());
  }
}

