import moment from "moment";
import {IS_NULL} from "../Core/Helpers/Helpers.misc";
import {TIME_PERIODS} from "./FC_Constants";

export const

  AS_TIMESTAMP = (val: any) => moment(IS_NULL(val) ? new Date() : val).unix() * 1000,

  START_OF = (val: any, timePeriod: any = TIME_PERIODS.MONTH) => moment(AS_TIMESTAMP(val)).startOf(timePeriod);