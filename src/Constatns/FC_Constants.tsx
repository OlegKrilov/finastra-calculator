export const API_ROUTES = {
  CALCULATE: 'calculate'
};

export const LOAN_TYPES = {
  PRIVATE_LOAN: 'privateLoan',
  CORPORATE_LOAN: 'corporateLoan',
  LEASING: 'leasing',
  DISCOUNTED_LOAN: 'discountedLoan',
  LETTERS_OF_CREDIT: 'lettersOfCredit',
  INVESTMENT: 'investment',
  OTHER: 'other'
};

export const MAIN_FORM_FIELDS = {
  AMOUNT_BORROWED_VALUE: 'amountBorrowedValue',
  AMOUNT_BORROWED_CURRENCY: 'amountBorrowedCurrency',
  START_DATE: 'startDate',
  INTEREST_RATE: 'interestRate',
  ADDITIONAL_INTEREST_RATE_DATE: 'additionalInterestRateDate',
  ADDITIONAL_INTEREST_RATE_VALUE: 'additionalInterestRateValue',
  DURATION_YEARS: 'durationYears',
  DURATION_MONTHS: 'durationMonths',
  RATE_BASIS: 'rateBasis',
  REPAYMENT_PLAN: 'repaymentPlan'
};

export const INPUT_TYPES = {
  STRING_INPUT: 'stringInput',
  NUMBER_INPUT: 'numberInput',
  DROPDOWN: 'dropdown',
  DATE_SELECTOR: 'dateSelector',
  RANGE_SELECTOR: 'rangeSelector'
};

export const TIME_PERIODS = {
  YEAR: 'year',
  MONTH: 'month',
  DAY: 'day'
};

export const COMMON = {
  USE_RANDOM_DATA: 'random_data'
};

