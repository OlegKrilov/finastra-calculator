import {CHECK_AND_CALL, DO_NOTHING} from "../Core/Helpers/Helpers.misc";

export const

  IS_VALID_NUMBER = (onFail = DO_NOTHING) => (val: any) => {
    let isValid = !isNaN(val);
    !isValid && CHECK_AND_CALL(onFail, val);
    return isValid;
  },

  NUMBER_IS_IN_RANGE = (min: number, max: number, onFail: any = DO_NOTHING) => (val: any) => {
    let isValid = false;

    if (!isNaN(val)) {
      let _val = parseFloat(val);
      isValid = _val >= min && _val <= max;
    }

    !isValid && CHECK_AND_CALL(onFail, val);
    return isValid;
  },

  INT_NUMBER = (onFail: any = DO_NOTHING) => (val: any) => {
    let isValid = parseFloat(val) === parseInt(val);
    !isValid && CHECK_AND_CALL(onFail, val);
    return isValid;
  };