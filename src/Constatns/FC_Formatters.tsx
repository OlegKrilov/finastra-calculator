import {EMPTY_STRING} from "../Core/Constants/ViewClasses.cnst";

export const
  BIG_NUMBER = (decimals: number = 2, fraction: number = 3) => (val: any) => {
    let
      _val = parseFloat(val).toFixed(decimals).split('.');

    let
      _len = _val[0].length,
      _int = _val[0].padStart(_len + (fraction - (_len % fraction)), 'Z').split(EMPTY_STRING),
      _str = '';

    while (_int.length) {
      _str += (_int.splice(0, fraction).join(EMPTY_STRING).replace(/Z*/, EMPTY_STRING) + ' ');
    }

    return `${_str.replace(/^\s/, EMPTY_STRING).replace(/\s$/, EMPTY_STRING)}.${_val[1]}`;
  };
