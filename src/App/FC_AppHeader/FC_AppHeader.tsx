import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {SharedService} from "../../Services/Shared.service";
import {FC_Button} from "../../Components/FC_Button/FC_Button";
import {IoMdDownload, IoMdArrowDropdown} from "react-icons/all";
import {FC_Dropdown} from "../../Components/FC_Dropdown/FC_Dropdown";

const
  ROOT = 'fc-app-header',
  BORDER = `${ROOT}-border`;

@inject('sharedService')
@observer
export class FC_AppHeader extends AbstractComponent {

  private readonly sharedService: SharedService;

  private readonly appTitle: string = 'CALCULATOR';

  render () {
    const
      {sharedService, appTitle} = this;

    return <div className={`${ROOT} row`}>
      <div className={'col-12'}>
        <h4>{appTitle}</h4>
      </div>
      <div className={`col-8`}>
        <div className={`row`}>
          <div className={`col-4 fc-block-height display-flex align-right`}>
            <span className={`padding-right-20`}>Type of Loan</span>
          </div>
          <div className={`col-3 fc-block-height display-flex align-left`}>
            <FC_Dropdown className={'full-width'} model={sharedService.loanTypeSelector} />
          </div>
        </div>
      </div>
      <div className={`col-4`}>
        <div className={`row`}>
          <div className={`col fc-block-height display-flex align-right`}>
            <FC_Button model={sharedService.downloadExcelButton} className={`margin-right-10`} children={
              <div className={`fc-color-blue`}>
                <IoMdDownload className={`margin-right-5`}/>
                <span>Download Excel file</span>
              </div>
            } />
            <FC_Button model={sharedService.shareButton} children={
              <div>
                <span className={`fc-color-blue`}>Share</span>
                <IoMdArrowDropdown className={`margin-left-5 fc-input-icon`}/>
              </div>
            } />
          </div>
        </div>
      </div>
      <div className={`col-12`}>
        <div className={`${BORDER}`} />
      </div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.sharedService = props.sharedService;
  }

}

