import React from "react";
import moment from "moment";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {DataService} from "../../Services/Data.service";
import {FC_GridModel, I_FC_GridColumn} from "../../Components/FC_Grid/FC_Grid.model";
import {COMPONENT, CONTENT, ICON, LABEL, NAME, VALUE, WIDTH} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {FC_Grid} from "../../Components/FC_Grid/FC_Grid";
import {GET_DEEP_PROPERTY} from "../../Core/Helpers/Helpers.misc";
import {BIG_NUMBER} from "../../Constatns/FC_Formatters";
import {SharedService} from "../../Services/Shared.service";
import {computed} from "mobx";
import {MAIN_FORM_FIELDS} from "../../Constatns/FC_Constants";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

const
  ROOT = 'fc-app-footer';

const
  DATE_FORMAT = 'DD MMMM YYYY';

const
  bigNumberFilter = BIG_NUMBER();


@inject('dataService', 'sharedService')
@observer
export class FC_AppFooter extends AbstractComponent {

  private readonly dataService: DataService;

  private readonly sharedService: SharedService;

  private readonly footerTitle: string = 'PAYMENT SCHEDULE';

  private readonly grid: FC_GridModel = new FC_GridModel().refreshCols([
      {
        [NAME]: 'first',
        [LABEL]: 
          <div className={`text-center full-width padding-h-10`}>
            <b>#</b>
          </div>,
        [CONTENT]: d =>
          <div className={`text-center full-width padding-h-10`}>
            <span>{d['index']}</span>
          </div>,
        [WIDTH]: 2
      },
      {
        [NAME]: 'second',
        [LABEL]:
          <div className={`text-left full-width padding-h-10`}>
            <b>Date</b>
          </div>,
        [CONTENT]: d =>
          <div className={`text-left full-width padding-h-10`}>
            <span>{moment(d['date']).format(DATE_FORMAT)}</span>
          </div>,
        [WIDTH]: 3
      },
      {
        [NAME]: 'third',
        [LABEL]:
          <div className={`text-left full-width padding-h-10`}>
            <b>Capital Left</b>
          </div>,
        [CONTENT]: d =>
          <div className={`text-left full-width padding-h-10`}>
            <span>{`${this.currencyIcon} ${bigNumberFilter(d['capitalLeft'])}`}</span>
          </div>,
        [WIDTH]: 3
      },
      {
        [NAME]: 'fourth',
        [LABEL]:
          <div className={`text-left full-width padding-h-10`}>
            <b>Interest</b>
          </div>,
        [CONTENT]: d =>
          <div className={`text-left full-width padding-h-10`}>
            <span>{`${this.currencyIcon} ${bigNumberFilter(d['interest'])}`}</span>
          </div>,
        [WIDTH]: 3
      },
      {
        [NAME]: 'fifth',
        [LABEL]:
          <div className={`text-left full-width padding-h-10`}>
            <b>Principal</b>
          </div>,
        [CONTENT]: d =>
          <div className={`text-left full-width padding-h-10`}>
            <span>{`${this.currencyIcon} ${bigNumberFilter(d['principal'])}`}</span>
          </div>,
        [WIDTH]: 3
      }
    ] as I_FC_GridColumn[]);

  @computed
  private get currencyIcon () {
    return GET_DEEP_PROPERTY(
      this.sharedService.mainForm.getItem(MAIN_FORM_FIELDS.AMOUNT_BORROWED_CURRENCY),
      COMPONENT,
      'currentValue',
      ICON
    ) || EMPTY_STRING;
  }

  componentDidMount(): void {
    const
      {grid, dataService} = this;

    const
      _refreshView = () => grid.refreshRows(
        GET_DEEP_PROPERTY(dataService.data, VALUE, 'values') || []
      );

    this.registerSubscriptions(
      dataService.data.getSubscription(
        () => _refreshView()
      )
    );
  }

  render () {
    const {footerTitle, grid} = this;

    return <div className={`${ROOT} row`}>
      <div className={'col-12'}>
        <h4>{footerTitle}</h4>
      </div>
      <div className={'col-12'}>
        <FC_Grid model={grid} />
      </div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.dataService = props.dataService;
    this.sharedService = props.sharedService;
  }

}

