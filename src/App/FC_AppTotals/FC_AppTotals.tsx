import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {DataService} from "../../Services/Data.service";
import {GET_DEEP_PROPERTY} from "../../Core/Helpers/Helpers.misc";
import {COLOR, COMPONENT, ICON, LABEL, NAME, VALUE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {BIG_NUMBER} from "../../Constatns/FC_Formatters";
import {computed} from "mobx";
import {MAIN_FORM_FIELDS} from "../../Constatns/FC_Constants";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {SharedService} from "../../Services/Shared.service";
import {FC_RoundChart} from "../../Components/FC_RoundChart/FC_RoundChart";
import {FC_RoundChartModel} from "../../Components/FC_RoundChart/FC_RoundChart.model";
import {IoMdSquare} from "react-icons/all";

const
  ROOT = 'fc-app-totals',
  VALUES_PANEL = `${ROOT}-values`,
  VISUALIZATION_PANEL = `${ROOT}-visualization`,
  CHART = `${VISUALIZATION_PANEL}-chart`;

const
  INTEREST_COLOR = '#FF8842',
  PRINCIPAL_COLOR = '#4A90E2';

@inject('dataService', 'sharedService')
@observer
export class FC_AppTotals extends AbstractComponent {

  private readonly dataService: DataService;

  private readonly sharedService: SharedService;

  private readonly chart: FC_RoundChartModel = new FC_RoundChartModel();

  componentDidMount(): void {
    const {sharedService, dataService, chart} = this;

    this.registerSubscriptions(
      dataService.data.getSubscription(
        () => {
          let totals = GET_DEEP_PROPERTY(dataService.data, VALUE, 'totals');
          chart.refreshItems(totals ? [
            {
              [NAME]: 'interest',
              [LABEL]: 'Interest',
              [COLOR]: INTEREST_COLOR,
              [VALUE]: totals.interest
            },
            {
              [NAME]: 'principal',
              [LABEL]: 'Principal',
              [COLOR]: PRINCIPAL_COLOR,
              [VALUE]: totals.principal
            }
          ] : []);
        }
      )
    );
  }

  @computed
  private get currencyIcon () {
    return GET_DEEP_PROPERTY(
      this.sharedService.mainForm.getItem(MAIN_FORM_FIELDS.AMOUNT_BORROWED_CURRENCY),
      COMPONENT,
      'currentValue',
      ICON
    ) || EMPTY_STRING;
  }

  render () {
    const
      bigNumberFilter = BIG_NUMBER(2),
      data = GET_DEEP_PROPERTY(this.dataService.data, VALUE, 'totals') || {};

    return <div className={`${ROOT} padding-v-25`}>

      <div className={`${VALUES_PANEL} padding-h-40 padding-v-10 fc-background-grey-light`}>

        <div className={`row`}>
          <div className={`col-12`}>
            <div className={`fc-row-height display-flex align-left`}>
              <b>TOTAL:</b>
            </div>
          </div>
        </div>

        <div className={`row`}>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-left`}>
              <span>APR</span>
            </div>
          </div>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-right`}>
              <span>{`${bigNumberFilter(data.interestPercentage)} %`}</span>
            </div>
          </div>
          <div className={`col-12 fc-border-bottom`}/>
        </div>

        <div className={`row`}>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-left`}>
              <span>Interest</span>
            </div>
          </div>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-right`}>
              <span>{`${this.currencyIcon} ${bigNumberFilter(data.interest)}`}</span>
            </div>
          </div>
          <div className={`col-12 fc-border-bottom`}/>
        </div>

        <div className={`row`}>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-left`}>
              <span>Principal</span>
            </div>
          </div>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-right`}>
              <span>{`${this.currencyIcon} ${bigNumberFilter(data.principal)}`}</span>
            </div>
          </div>
          <div className={`col-12 fc-border-bottom`}/>
        </div>

        <div className={`row`}>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-left`}>
              <span>Annuity</span>
            </div>
          </div>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-right`}>
              <span>{`${this.currencyIcon} ${bigNumberFilter(data.annuity)}`}</span>
            </div>
          </div>
          <div className={`col-12 fc-border-bottom`}/>
        </div>

        <div className={`row`}>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-left`}>
              <span>Monthly Payment</span>
            </div>
          </div>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-right`}>
              <span>{`${this.currencyIcon} ${bigNumberFilter(data.monthlyPayment)}`}</span>
            </div>
          </div>
          <div className={`col-12 fc-border-bottom`}/>
        </div>

        <div className={`row`}>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-left`}>
              <span>Months</span>
            </div>
          </div>
          <div className={'col-6'}>
            <div className={`fc-row-height display-flex align-right`}>
              <span>{data.months}</span>
            </div>
          </div>
        </div>

      </div>

      <div className={`${VISUALIZATION_PANEL} padding-h-40 padding-v-10 fc-background-grey-light margin-top-10`}>
        <div className={`row`}>
          <div className={`col-5`}>
            <FC_RoundChart className={`${CHART}`} model={this.chart} />
          </div>
          <div className={`col-7`}>
            <div className={`display-flex direction-column align-center size-cover`}>
              <div className={`full-width`}>
                <IoMdSquare className={`margin-right-5`} style={{'color': PRINCIPAL_COLOR}}/>
                <div className={`display-inline-flex`}>Principal {bigNumberFilter(data.principalPercentage || 0)}%</div>
              </div>
              <div className={`full-width`}>
                <IoMdSquare className={`margin-right-5`} style={{'color': INTEREST_COLOR}}/>
                <div className={`display-inline-flex`}>Interest {bigNumberFilter(data.interestPercentage || 0)}%</div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>;
  }

  constructor (props) {
    super(props);
    this.dataService = props.dataService;
    this.sharedService = props.sharedService;
  }

}

