import React from "react";
import {AbstractComponent} from "../Core/Abstract/AbstractComponent";
import {FC_AppHeader} from "./FC_AppHeader/FC_AppHeader";
import {inject, observer} from "mobx-react";
import {FC_AppMain} from "./FC_AppMain/FC_AppMain";
import {I_FC_MainFormField, SharedService} from "../Services/Shared.service";
import {IS_NOT_NULL} from "../Core/Helpers/Helpers.misc";
import {DataService} from "../Services/Data.service";
import {COMPONENT} from "../Core/Constants/PropertiesAndAttributes.cnst";
import {FC_AppTotals} from "./FC_AppTotals/FC_AppTotals";
import {FC_AppFooter} from "./FC_AppFooter/FC_AppFooter";
import {BIG_NUMBER} from "../Constatns/FC_Formatters";
import {FC_Loader} from "../Components/FC_Loader/FC_Loader";

const
  ROOT = 'app-root';

@inject('sharedService', 'dataService')
@observer
export class FC_App extends AbstractComponent {

  private readonly sharedService: SharedService;

  private readonly dataService: DataService;

  componentDidMount(): void {
    const
      {sharedService, dataService} = this,
      {loanTypeSelector, formWasSubmitted, isLoading} = sharedService;

    const _calculate = () => dataService.calculate(sharedService.formValue)
      .then(res => console.log(res))
      .catch(err => console.log(err));

    this.registerSubscriptions(
      loanTypeSelector.onChange.getSubscription(
        () => {
          sharedService.clearForm();
          dataService.data.resetValue();
          formWasSubmitted.toggleValue(false);
        }
      ),

      formWasSubmitted.getSubscription(
        state => {
          if (state) {
            isLoading.toggleValue(true);
            _calculate().finally(() => isLoading.setValue(false));
          }
        }
      ),

      // sharedService.downloadExcelButton.onClick.getSubscription(
      //   () => sharedService.isLoading.toggleValue()
      // )
    );

    setTimeout(() => isLoading.toggleValue(), 2000);
  }

  render () {
    const
      {sharedService} = this,
      loanIsSelected = IS_NOT_NULL(this.sharedService.loanTypeSelector.selection),
      hasLoadedData = this.dataService.hasLoadedData;

    return <div className={`${ROOT} container-fluid padding-h-100`}>
      <FC_AppHeader />

      {!loanIsSelected && <div className={`row`}>
        <h3 className={`col-12 opacity-5 margin-v-20`}>Select Loan Type</h3>
      </div>}

      <div className={`row`}>
        <div className={`col-8`}>
          {loanIsSelected && <FC_AppMain />}
        </div>
        <div className={`col-4`}>
          {hasLoadedData && <FC_AppTotals />}
        </div>
      </div>

      <div className={`row`}>
        <div className={`col-12`}>
          {hasLoadedData && <FC_AppFooter />}
        </div>
      </div>

      <FC_Loader isOpened={sharedService.isLoading} />

    </div>;
  }

  constructor (props) {
    super(props);
    this.sharedService = props.sharedService;
    this.dataService = props.dataService;
  }

}