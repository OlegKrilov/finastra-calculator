import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {I_FC_MainFormField, SharedService} from "../../Services/Shared.service";
import {GET_DEEP_PROPERTY, IS_NULL} from "../../Core/Helpers/Helpers.misc";
import {MAIN_FORM_FIELDS} from "../../Constatns/FC_Constants";
import {FC_NumberInput} from "../../Components/FC_NumberInput/FC_NumberInput";
import {COMPONENT, ID} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {FC_Dropdown} from "../../Components/FC_Dropdown/FC_Dropdown";
import {FC_DateSelector} from "../../Components/FC_DateSelector/FC_DateSelector";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import {FC_ButtonModel} from "../../Components/FC_Button/FC_Button.model";
import {EMPTY_STRING, IS_REQUIRED} from "../../Core/Constants/ViewClasses.cnst";
import {FC_Button} from "../../Components/FC_Button/FC_Button";
import {IoMdAddCircle, IoMdCloseCircle} from "react-icons/all";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {DataService} from "../../Services/Data.service";

const
  ROOT = 'fc-app-main',
  FORM = `${ROOT}-form`;

@inject('sharedService', 'dataService')
@observer
export class FC_AppMain extends AbstractComponent {

  private readonly dataService: DataService;

  private readonly sharedService: SharedService;

  private readonly enableAdditionalInterestFields: FC_ButtonModel = new FC_ButtonModel();

  private readonly useAdditionalInterest: SwitchObservable = new SwitchObservable();

  private readonly calculateButton: FC_ButtonModel = new FC_ButtonModel().toggleDisabled(true);

  private readonly clearAllButton: FC_ButtonModel = new FC_ButtonModel().toggleDisabled(true);
  
  private _getField = (fieldName, prop: string | null | any = null) => {
    let field = this.sharedService.mainForm.getItem(fieldName);
    return IS_NULL(prop) ? field : GET_DEEP_PROPERTY(field, prop);
  };

  componentDidMount(): void {
    const
      {
        sharedService,
        dataService,
        useAdditionalInterest,
        enableAdditionalInterestFields,
        calculateButton,
        clearAllButton
      } = this,
      {
        loanTypeSelector,
        shareButton,
        downloadExcelButton,
        formWasSubmitted,
        mainForm
      } = sharedService,
      periodFields = [MAIN_FORM_FIELDS.DURATION_YEARS, MAIN_FORM_FIELDS.DURATION_MONTHS],
      additionalFormFields = [MAIN_FORM_FIELDS.ADDITIONAL_INTEREST_RATE_DATE, MAIN_FORM_FIELDS.ADDITIONAL_INTEREST_RATE_VALUE];

    this.registerSubscriptions(

      loanTypeSelector.onChange.getSubscription(
        () => useAdditionalInterest.toggleValue(false)
      ),

      enableAdditionalInterestFields.onClick.getSubscription(
        () => useAdditionalInterest.toggleValue(true)
      ),

      calculateButton.onClick.getSubscription(
        () => formWasSubmitted.toggleValue(true)
      ),

      clearAllButton.onClick.getSubscription(
        () => {
          dataService.data.resetValue();
          sharedService.clearForm();
          formWasSubmitted.resetValue();
        }
      ),

      useAdditionalInterest.getSubscription(
        state => state ?
          additionalFormFields
            .forEach(fieldName => {
              let
                field = mainForm.getItem(fieldName),
                isRequired = GET_DEEP_PROPERTY(field, IS_REQUIRED);
              isRequired && isRequired.setValue(true);
            }) :
          sharedService.clearForm((field) => additionalFormFields.includes(field[ID]))
      ),

      formWasSubmitted.getSubscription(
        state => {
          mainForm.items.forEach(
            (d: I_FC_MainFormField) => d[COMPONENT].toggleDisabled(state)
          );
          enableAdditionalInterestFields.toggleDisabled(state);
          clearAllButton.toggleDisabled(!state);
        }
      ),

      /** Period observer */
      new AbstractObserver(() => periodFields.map(
        (fieldName: string) => {
          const field: I_FC_MainFormField = mainForm.getItem(fieldName);
          return field && GET_DEEP_PROPERTY(field[COMPONENT], 'currentValue')
        }
      )).getSubscription(
        () => {
          const
            monthsField: I_FC_MainFormField = mainForm.getItem(MAIN_FORM_FIELDS.DURATION_MONTHS),
            yearsField: I_FC_MainFormField = mainForm.getItem(MAIN_FORM_FIELDS.DURATION_YEARS),
            monthsRequired: SwitchObservable = GET_DEEP_PROPERTY(monthsField, IS_REQUIRED),
            yearsRequired: SwitchObservable = GET_DEEP_PROPERTY(yearsField, IS_REQUIRED),
            numberOfMonths: number = GET_DEEP_PROPERTY(monthsField, COMPONENT,'currentValue'),
            numberOfYears: number = GET_DEEP_PROPERTY(yearsField, COMPONENT, 'currentValue');

          monthsRequired && monthsRequired.setValue(!numberOfYears);
          yearsRequired && yearsRequired.setValue(!numberOfMonths);
        }
      ),

      /** Validity observer */
      new AbstractObserver(() => [sharedService.isValid, formWasSubmitted.value]).getSubscription(
        () => calculateButton.toggleDisabled(formWasSubmitted.value || !sharedService.isValid)
      )
    );
  }

  render () {
    const
      {sharedService, useAdditionalInterest, enableAdditionalInterestFields, calculateButton, clearAllButton} = this,
      {formWasSubmitted} = sharedService;

    return <div className={`${ROOT} padding-v-15`}>
      <div className={`row`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>Amount Borrowed</div>
        </div>
        <div className={`col-3`}>
          <FC_NumberInput model={this._getField(MAIN_FORM_FIELDS.AMOUNT_BORROWED_VALUE, COMPONENT)} />
        </div>
        <div className={`col-3`}>
          <FC_Dropdown model={this._getField(MAIN_FORM_FIELDS.AMOUNT_BORROWED_CURRENCY, COMPONENT)} />
        </div>
      </div>
      <div className={`row margin-v-10`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>Start Date of the Loan</div>
        </div>
        <div className={`col-3`}>
          <FC_DateSelector model={this._getField(MAIN_FORM_FIELDS.START_DATE, COMPONENT)} />
        </div>
      </div>
      <div className={`row margin-v-10`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>Interest Rate</div>
        </div>
        <div className={`col-3`}>
          <FC_NumberInput model={this._getField(MAIN_FORM_FIELDS.INTEREST_RATE, COMPONENT)} />
        </div>
      </div>
      <div className={`row margin-v-10`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>{
            useAdditionalInterest.value ? 'Additional Interest Rate' : EMPTY_STRING}
          </div>
        </div>
        <div className={`col-3`}>{
          useAdditionalInterest.value ?
            <FC_DateSelector model={this._getField(MAIN_FORM_FIELDS.ADDITIONAL_INTEREST_RATE_DATE, COMPONENT)} /> :
            <FC_Button model={enableAdditionalInterestFields} children={
              <div className={`fc-color-blue`}>
                <IoMdAddCircle className={`margin-right-5`}/>
                <span>Add more interest rate</span>
              </div>
            } />
        }</div>
        <div className={`col-2`}>{useAdditionalInterest.value &&
          <FC_NumberInput model={this._getField(MAIN_FORM_FIELDS.ADDITIONAL_INTEREST_RATE_VALUE, COMPONENT)} />
        }</div>
        <div className={`col-2`}>
          {useAdditionalInterest.value && !formWasSubmitted.value &&
            <div className={`fc-color-red fc-row-height display-inline-flex align-center fc-clickable padding-left-10`}
                onClick={() => useAdditionalInterest.toggleValue(false)}>
              <IoMdCloseCircle className={`margin-right-5`}/>
              <span>Remove</span>
            </div>
          }
        </div>
      </div>
      <div className={`row margin-v-10`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>Duration</div>
        </div>
        <div className={`col-3`}>
          <FC_NumberInput model={this._getField(MAIN_FORM_FIELDS.DURATION_YEARS, COMPONENT)} />
        </div>
        <div className={`col-3`}>
          <FC_NumberInput model={this._getField(MAIN_FORM_FIELDS.DURATION_MONTHS, COMPONENT)} />
        </div>
      </div>
      <div className={`row margin-v-10`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>Rate Basis</div>
        </div>
        <div className={`col-3`}>
          <FC_Dropdown model={this._getField(MAIN_FORM_FIELDS.RATE_BASIS, COMPONENT)} />
        </div>
      </div>
      <div className={`row margin-v-10`}>
        <div className={`col-4 text-right`}>
          <div className={'fc-row-height display-flex align-right padding-right-20'}>Repayment Plan</div>
        </div>
        <div className={`col-3`}>
          <FC_Dropdown model={this._getField(MAIN_FORM_FIELDS.REPAYMENT_PLAN, COMPONENT)} />
        </div>
      </div>
      <div className={`row`}>
        <div className={`col-4`} />
        <div className={`col-8`}>
          <FC_Button model={calculateButton} className={`fc-bg-blue fc-color-white margin-right-10`} children={
            <div className={`padding-h-15`}>CALCULATE</div>
          } />
          <FC_Button model={clearAllButton} className={`fc-bg-grey fc-color-white margin-right-10`} children={
            <div className={`padding-h-15`}>CLEAR</div>
          } />
        </div>
      </div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.sharedService = props.sharedService;
    this.dataService = props.dataService;
  }

}

