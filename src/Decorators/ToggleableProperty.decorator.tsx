import {CAPITALIZE} from "../Core/Helpers/Helpers.misc";
import {SwitchObservable} from "../Core/Observables/SwitchObservable";

export const ToggleableProperty = (prop) => (target: any) => {

  const
    CAPITALIZED = CAPITALIZE(prop),
    PROP_NAME = `is${CAPITALIZED}`,
    TOGGLE_PROP = `toggle${CAPITALIZED}`;

  const f = function ToggleablePropertyConstructor (...args) {
    Object.defineProperties(target.prototype, {
      [PROP_NAME]: {
        value: new SwitchObservable()
      },
      [TOGGLE_PROP]: {
        value: (state: boolean | null = null) => {
          target.prototype[PROP_NAME].toggleValue(state);
          return target;
        }
      }
    });

    return new target(...args);
  };

  f.prototype = target.prototype;
  return f;
};