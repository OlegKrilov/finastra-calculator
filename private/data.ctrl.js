const
  d3 = require('d3'),
  moment = require('moment');

const
  CONSTANTS = require('./contants'),
  HELPERS = require('./helpers');

const
  generateData = (form) => {
    const
      data = {
        totals: {
          principalPercentage: 0,
          principal: 0,
          interestPercentage: 0,
          interest: 0,
          annuity: 0,
          monthlyPayment: 0,
          months: 0
        },
        values: []
      },
      {totals, values} = data;

    totals.months = (form['durationYears'] * 12) + form['durationMonths'];
    totals.principal = HELPERS.GET_RANDOM_FLOAT(0, 999999);
    totals.interestPercentage = form['interestRate'];
    totals.principalPercentage = 100 - totals.interestPercentage;
    totals.interest = (totals.principal / 100) * totals.interestPercentage;
    totals.annuity = totals.interest + totals.principal;
    totals.monthlyPayment = totals.annuity / totals.months;

    let
      baseDate = moment(form['startDate']),
      stepInterest = (totals.monthlyPayment / 100) * totals.interestPercentage,
      stepPrincipal = totals.monthlyPayment - stepInterest;

    d3.range(totals.months).forEach(i => values.push({
      index: i,
      capitalLeft: totals.annuity - (totals.monthlyPayment * (i + 1)),
      date: new Date(moment(baseDate).add(i, 'month').endOf('month').unix() * 1000),
      interest: stepInterest,
      principal: stepPrincipal
    }));

    return data;
  };

module.exports = (app) => {

  app.post(CONSTANTS.API.CALCULATE, (req, res) => res.send(generateData(req.body)));

};