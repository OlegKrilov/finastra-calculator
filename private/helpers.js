const
  GET_RANDOM = (val = 1, allowNegative = false) => Math.random() * val * (allowNegative ? (Math.random() > .5 ? -1 : 1) : 1),

  GET_RANDOM_INT = (min = 0, max = 1, allowNegative = false) => Math.round(min + GET_RANDOM(max, allowNegative));

  GET_RANDOM_FLOAT = (min = 0, max = 1, decimals = 2, allowNegative = false) => GET_RANDOM_INT(
    min, max * Math.pow(10, decimals), allowNegative
  ) / Math.pow(10, decimals);

module.exports = {
  GET_RANDOM,
  GET_RANDOM_INT,
  GET_RANDOM_FLOAT



};


// import {EMPTY_STRING} from "../src/Core/Constants/ViewClasses.cnst";
// import {CAPITALIZE, CHARS, RANDOM_WORDS} from "../src/Core/Helpers/Helpers.misc";
//
// export const GET_RANDOM = (val: number = 1, allowNegative: boolean = false) => (Math.random() * val) *
//   (allowNegative ? [-1, 1][GET_RANDOM_INT()] : 1);
//
// export const GET_RANDOM_INT = (val: number = 2) => Math.floor(GET_RANDOM(val));
//
// export const GET_RANDOM_BETWEEN = (fromVal: number = 0, toVal: number = 1) => fromVal + GET_RANDOM(toVal - fromVal);
//
// export const GET_RANDOM_FROM_ARRAY = (arr: any[]) => arr[Math.floor(GET_RANDOM(arr.length))];
//
// export const GET_RANDOM_WORD = () => RANDOM_WORDS[Math.floor(GET_RANDOM(RANDOM_WORDS.length))];
//
// export const GET_RANDOM_STRING = (n: number = 0) => {
//   let _n = n || (1 + GET_RANDOM_INT(10));
//   return CAPITALIZE(d3.range(_n).map(i => `${GET_RANDOM_WORD()}${_n - 1 === i ? '.' : EMPTY_STRING} `).join(''));
// };
//
// export const GET_RANDOM_TEXT = (n: number = 0) => {
//   let _n = n || (1 + GET_RANDOM_INT(10));
//   return `${d3.range(_n).map(GET_RANDOM_STRING).join('')}\r\n`;
// };
//
// export const GET_RANDOM_CHAR = () => {
//   let len = CHARS.length;
//   return CHARS[GET_RANDOM_INT(len)][GET_RANDOM_INT() ? 'toUpperCase' : 'toLowerCase']();
// };
//
// export const GET_RANDOM_CHARS = (n: number = 0) => {
//   let _n = n < 1 ? 3 + GET_RANDOM_INT(10) : n;
//   return d3.range(_n).map(() => GET_RANDOM_CHAR()).join(EMPTY_STRING);
// };